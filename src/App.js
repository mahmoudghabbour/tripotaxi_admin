import { BrowserRouter, Routes, Route } from "react-router-dom";
import { connect } from "react-redux";
import SignIn from "./components/auth/SignIn";
import DriverDetails from "./components/drivers/DriverDetails";
import DashboardAllDrivers from "./components/dashboard/DashboardAllDrivers";
import MainPage from "./components/layout/MainPage";
import Navbar from "./components/layout/Navbar";
import DashboardAllUsers from "./components/dashboard/DashboardAllUsers";
import UserDetails from "./components/users/UserDetails";
import DashboardBlockedUsers from "./components/dashboard/DashboardBlockedUsers";
import DashboardBlockedDrivers from "./components/dashboard/DashboardBlockedDrivers";
import DashboardAllTrips from "./components/dashboard/DashboardAllTrips";
import DashboardTripsByDriver from "./components/dashboard/DashboardTripsByDriver";
import DashboardTripsByPassenger from "./components/dashboard/DashboardTripsByPassenger";
import DashboardTripsCategories from "./components/dashboard/DashboardTripsCategories";
import DashboardAllEvents from "./components/dashboard/DashboardAllEvents";

function App(props) {
  const token = localStorage.getItem("userToken");
  return (
    <BrowserRouter>
      {token || props.auth.auth ? <Navbar /> : null}
      <Routes>
        <Route path="/" element={<SignIn />} />
        {/* <Route path="/testPage" element={<TestPage />} />
        <Route path="/admindrawer" element={<AdminDrawer />} /> */}
        <Route path="/adminuserprofile/:id" element={<UserDetails />} />
        <Route path="/admindriverprofile/:id" element={<DriverDetails />} />
        <Route
          path="/mainpage/dashboardallevents"
          element={<DashboardAllEvents />}
        />
        <Route
          path="/mainpage/dashboardtripscategories"
          element={<DashboardTripsCategories />}
        />
        <Route
          path="/mainpage/tripsbydriver"
          element={<DashboardTripsByDriver />}
        />
        <Route
          path="/mainpage/tripsbypassenger"
          element={<DashboardTripsByPassenger />}
        />
        <Route path="/mainpage/alltrips" element={<DashboardAllTrips />} />
        <Route
          path="/mainpage/allblockeddriverslist"
          element={<DashboardBlockedDrivers />}
        />
        <Route
          path="/mainpage/allblockeduserslist"
          element={<DashboardBlockedUsers />}
        />
        <Route path="/mainpage/alldrivers" element={<DashboardAllDrivers />} />
        <Route path="/mainpage/allusers" element={<DashboardAllUsers />} />
        <Route path="/mainpage" element={<MainPage />} />
      </Routes>
    </BrowserRouter>
  );
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.auth,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
