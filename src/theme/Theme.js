import { createTheme } from "@mui/material/styles";

export default createTheme({
  palette: {
    primary: {
      main: "#2DBB54",
      light: "#C3CDD9",
      primaryText: "#303030",
      secondaryText: "#D6D6D6",
      icons: "#FFFFFF",
      background: "#FFFFFF",
      errColor: "red",
    },
  },
  typography: {
    h1: {
      fontFamily: "Poppins",
      fontWeight: 300,
      fontSize: "6rem",
      lineHeight: 1.167,
    },
    h2: {
      fontFamily: "Poppins",
      fontWeight: 300,
      fontSize: "3.75rem",
      lineHeight: 1.2,
    },
    h3: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "3rem",
      lineHeight: 1.167,
    },
    h4: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "2.125rem",
      lineHeight: 1.235,
    },
    h5: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "1.5rem",
      lineHeight: 1.334,
    },
    h6: {
      fontFamily: "Poppins",
      fontWeight: 500,
      fontSize: "1.25rem",
      lineHeight: 1.6,
    },
    subtitle1: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "1rem",
      lineHeight: 1.75,
    },
    subtitle2: {
      fontFamily: "Poppins",
      fontWeight: 500,
      fontSize: "0.875rem",
      lineHeight: 1.57,
    },
    body1: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "1rem",
      lineHeight: 1.5,
    },
    body2: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "0.875rem",
      lineHeight: 1.43,
    },
    // button: {
    //   fontFamily: "Poppins",
    //   fontWeight: 500,
    //   fontSize: "0.875rem",
    //   lineHeight: 1.75,
    // },
    caption: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "0.75rem",
      lineHeight: 1.66,
    },
    overline: {
      fontFamily: "Poppins",
      fontWeight: 400,
      fontSize: "0.75rem",
      lineHeight: 2.66,
    },
  },
});
