import React, { Component } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker,
  Polyline,
} from "react-google-maps";
import Grid from "@mui/material/Grid";
import { CircularProgress } from "@mui/material";
import { constantsConfig } from "../../config/ConstantsConfig";
// import ic_my_location from "./";
// import { Component } from "react/cjs/react.production.min";

const path = [
  { lat: 33.522054, lng: 36.279137 },
  { lat: 33.524743, lng: 36.281241 },
];

const options = {
  strokeColor: "#303030",
  strokeOpacity: 0.8,
  strokeWeight: 2,
  fillColor: "#303030",
  fillOpacity: 0.35,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000,
  paths: [
    { lat: 33.522054, lng: 36.279137 },
    { lat: 33.524743, lng: 36.281241 },
  ],
  zIndex: 1,
};

const pathCoordinates = [
  { lat: 33.522054, lng: 36.279137 },
  { lat: 33.522054, lng: 36.279137 },
];
const MapRender = withScriptjs(
  withGoogleMap((props) => {
    var i = 0;
    return (
      <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: props.paths[0].lat, lng: props.paths[0].lng }}
      >
        {props.isMarkerShown && (
          <Marker
            icon={{
              url: require("./unnamed.png"),

              // anchor: new window.google.maps.Point(17, 46),

              scaledSize: new window.google.maps.Size(37, 37),
            }}
            position={{ lat: props.paths[0].lat, lng: props.paths[0].lng }}
            label={{ text: "A", color: "#fff" }}
          />
        )}
        <Polyline
          // path={pathCoordinates}
          path={props.paths}
          options={options}
        />
        {props.paths &&
          props.paths.map((path) => {
            i += 1;
            if (i % 8 === 0)
              return (
                <Marker
                  icon={{
                    // url: require("./ic_my_location.png"),

                    anchor: new window.google.maps.Point(17, 46),

                    scaledSize: new window.google.maps.Size(50, 50),
                  }}
                  position={{
                    lat: path.lat,
                    lng: path.lng,
                  }}
                  label={{ text: path.speed.toString(), color: "#fff" }}
                />
              );
            else return null;
          })}
        {props.isMarkerShown && (
          <Marker
            icon={{
              url: require("./unnamed.png"),

              // anchor: new window.google.maps.Point(17, 46),

              scaledSize: new window.google.maps.Size(50, 50),
            }}
            position={{
              lat: props.paths[props.paths.length - 1].lat,
              lng: props.paths[props.paths.length - 1].lng,
            }}
            label={{ text: "B", color: "#fff" }}
          />
        )}
      </GoogleMap>
    );
  })
);

class MapComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paths: null,
    };
  }
  componentDidMount() {
    var paths = [];
    for (let i = 0; i < this.props.tripHistory.length; i++) {
      var lat = parseFloat(this.props.tripHistory[i][1]);
      var lng = parseFloat(this.props.tripHistory[i][2]);
      var speed = parseFloat(this.props.tripHistory[i][5]);
      paths.push({ lat, lng, speed });
    }
    this.setState({
      paths: paths,
    });
  }
  render() {
    return (
      <MapRender
        isMarkerShown
        googleMapURL={
          "https://maps.googleapis.com/maps/api/js?key=" +
          constantsConfig.apiKey +
          "&libraries=places"
        }
        loadingElement={
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <CircularProgress />
          </Grid>
        }
        containerElement={<div style={{ height: `400px`, padding: 20 }} />}
        mapElement={<div style={{ height: `100%` }} />}
        paths={this.state.paths}
      />
    );
  }
}

export default MapComponent;
