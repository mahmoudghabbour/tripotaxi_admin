import React, { Component } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker,
  Polyline,
} from "react-google-maps";
import Grid from "@mui/material/Grid";
import { CircularProgress } from "@mui/material";
import { constantsConfig } from "../../config/ConstantsConfig";
// import ic_my_location from "./";
// import { Component } from "react/cjs/react.production.min";

const path = [
  { lat: 33.522054, lng: 36.279137 },
  { lat: 33.524743, lng: 36.281241 },
];

const pathCoordinates = [
  { lat: 33.522054, lng: 36.279137 },
  { lat: 33.522054, lng: 36.279137 },
];
const MapRender = withScriptjs(
  withGoogleMap((props) => {
    const options = {
      strokeColor: "#303030",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#303030",
      fillOpacity: 0.35,
      clickable: false,
      draggable: false,
      editable: false,
      visible: true,
      radius: 30000,
      paths: props.paths,
      zIndex: 1,
    };
    return (
      <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: 33.522054, lng: 36.279137 }}
      >
        {props.isMarkerShown && (
          <Marker
            icon={{
              url: require("./unnamed.png"),

              // anchor: new window.google.maps.Point(17, 46),

              scaledSize: new window.google.maps.Size(37, 37),
            }}
            position={{ lat: 33.522054, lng: 36.279137 }}
            label={{ text: "A", color: "#fff" }}
          />
        )}

        {props.isMarkerShown && (
          <Marker
            icon={{
              url: require("./unnamed.png"),

              // anchor: new window.google.maps.Point(17, 46),

              scaledSize: new window.google.maps.Size(50, 50),
            }}
            position={{ lat: 33.524743, lng: 36.281241 }}
            label={{ text: "B", color: "#fff" }}
          />
        )}
        {props.isMarkerShown && (
          <Marker
            icon={{
              url: require("./car_above_icon_parked_green.png"),

              // anchor: new window.google.maps.Point(17, 46),

              scaledSize: new window.google.maps.Size(30, 30),
            }}
            position={{
              lat: parseFloat(props.driverCarLocation.lat),
              lng: parseFloat(props.driverCarLocation.lng),
            }}
          />
        )}
      </GoogleMap>
    );
  })
);

class TripMapComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paths: null,
    };
  }
  componentDidMount() {
    // var paths = [];
    // // for (let i = 0; i < this.props.driverLocation.length; i++) {
    // //   var lat = parseFloat(this.props.driverLocation[i][1]);
    // //   var lng = parseFloat(this.props.driverLocation[i][2]);
    // //   paths.push({ lat, lng });
    // // }
    // this.setState({
    //   paths: paths,
    // });
  }
  render() {
    const { driverCarLocation } = this.props;
    return (
      <MapRender
        isMarkerShown
        googleMapURL={
          "https://maps.googleapis.com/maps/api/js?key=" +
          constantsConfig.apiKey +
          "&libraries=places"
        }
        loadingElement={
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <CircularProgress />
          </Grid>
        }
        containerElement={<div style={{ height: `400px`, padding: 20 }} />}
        mapElement={<div style={{ height: `100%` }} />}
        driverCarLocation={driverCarLocation}
        // paths={this.state.paths}
      />
    );
  }
}

export default TripMapComponent;
