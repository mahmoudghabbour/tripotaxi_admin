import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const styles = (theme) => ({});

class EventsList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { event } = this.props;
    return (
      <React.Fragment>
        <Card sx={{ minWidth: 275, marginTop: "0.5em" }}>
          <CardContent>
            <Typography
              sx={{
                fontSize: 14,
                color: (theme) => theme.palette.primary.main,
              }}
              color="text.secondary"
              gutterBottom
              align="right"
            >
              {event[1]}
            </Typography>
            <Typography variant="h5" component="div" align="right">
              {event[3]}
            </Typography>
            <Typography
              variant="h5"
              component="div"
              align="right"
              sx={{ color: (theme) => theme.palette.primary.main }}
            >
              {event[11]}
            </Typography>
            <Typography variant="body2" align="right">
              {event[12]}
            </Typography>
          </CardContent>
        </Card>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(EventsList);
