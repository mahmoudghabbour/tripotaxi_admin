import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import AdminDrawer from "./drawers/AdminDrawer";
import {
  getTodayInvoices,
  getLastWeekInvoices,
  getInvoicesThisMonth,
  getInvoicesLastMonth,
} from "../../store/actions/AllInvoicesAction";

const styles = (theme) => ({
  tableTitle: {
    color: theme.palette.primary.main,
  },
});

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dashboardAllDrivers: false,
      isFirstTime: false,
      companyRatio: 20,
      driverRatio: 80,
    };
  }

  componentDidMount() {
    this.props.getTodayInvoices();
    this.props.getLastWeekInvoices();
    this.props.getInvoicesThisMonth();
    this.props.getInvoicesLastMonth();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.invoicesLastMonth && !this.state.isFirstTime) {
      this.setState({
        isFirstTime: true,
      });
      // Revenue
      var invoicesToday = nextProps.invoicesToday;
      var invoicesLastWeek = nextProps.invoicesLastWeek;
      var invoicesThisMonth = nextProps.invoicesThisMonth;
      var invoicesLastMonth = nextProps.invoicesLastMonth;
      // Company Net Revenue
      var invoicesTodayCompany =
        (nextProps.invoicesToday * this.state.companyRatio) / 100;
      var invoicesLastWeekCompany =
        (nextProps.invoicesLastWeek * this.state.companyRatio) / 100;
      var invoicesThisMonthCompany =
        (nextProps.invoicesThisMonth * this.state.companyRatio) / 100;
      var invoicesLastMonthCompany =
        (nextProps.invoicesLastMonth * this.state.companyRatio) / 100;
      // Driver Net Revenue
      var invoicesTodayDriver =
        (nextProps.invoicesToday * this.state.driverRatio) / 100;
      var invoicesLastWeekDriver =
        (nextProps.invoicesLastWeek * this.state.driverRatio) / 100;
      var invoicesThisMonthDriver =
        (nextProps.invoicesThisMonth * this.state.driverRatio) / 100;
      var invoicesLastMonthDriver =
        (nextProps.invoicesLastMonth * this.state.driverRatio) / 100;

      this.setState({
        invoicesToday: invoicesToday,
        invoicesLastWeek: invoicesLastWeek,
        invoicesThisMonth: invoicesThisMonth,
        invoicesLastMonth: invoicesLastMonth,
        invoicesTodayCompany: invoicesTodayCompany,
        invoicesLastWeekCompany: invoicesLastWeekCompany,
        invoicesThisMonthCompany: invoicesThisMonthCompany,
        invoicesLastMonthCompany: invoicesLastMonthCompany,
        invoicesTodayDriver: invoicesTodayDriver,
        invoicesLastWeekDriver: invoicesLastWeekDriver,
        invoicesThisMonthDriver: invoicesThisMonthDriver,
        invoicesLastMonthDriver: invoicesLastMonthDriver,
      });
    }
  }

  handleOpenDashboardDrivers = () => {
    this.setState({
      dashboardAllDrivers: true,
    });
  };
  render() {
    const { classes } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <Box sx={{ display: "flex" }}>
          <AdminDrawer
            handleOpenDashboardDrivers={this.handleOpenDashboardDrivers}
          />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={5}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Revenue
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Today</TableCell>
                    <TableCell align="center">Last Week</TableCell>
                    <TableCell align="center">This month</TableCell>
                    <TableCell align="center">Last Month</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {this.state.invoicesToday}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastWeek}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesThisMonth}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastMonth}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={5}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Company Net Revenue
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Today</TableCell>
                    <TableCell align="center">Last Week</TableCell>
                    <TableCell align="center">This month</TableCell>
                    <TableCell align="center">Last Month</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {this.state.invoicesTodayCompany}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastWeekCompany}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesThisMonthCompany}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastMonthCompany}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={5}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Drivers Net Revenue
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Today</TableCell>
                    <TableCell align="center">Last Week</TableCell>
                    <TableCell align="center">This month</TableCell>
                    <TableCell align="center">Last Month</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {this.state.invoicesTodayDriver}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastWeekDriver}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesThisMonthDriver}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.invoicesLastMonthDriver}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    invoicesToday: state.allInvoices.invoicesToday,
    invoicesThisMonth: state.allInvoices.invoicesThisMonth,
    invoicesLastWeek: state.allInvoices.invoicesLastWeek,
    invoicesLastMonth: state.allInvoices.invoicesLastMonth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getTodayInvoices: () => dispatch(getTodayInvoices()),
    getLastWeekInvoices: () => dispatch(getLastWeekInvoices()),
    getInvoicesThisMonth: () => dispatch(getInvoicesThisMonth()),
    getInvoicesLastMonth: () => dispatch(getInvoicesLastMonth()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(MainPage);
