import * as React from "react";
import { Link } from "react-router-dom";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import AccountIcon from "@mui/icons-material/AccountBox";
import { makeStyles } from "@mui/styles";
import BlockedIcon from "@mui/icons-material/Block";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import GroupIcon from "@mui/icons-material/Group";
import DriveEtaIcon from "@mui/icons-material/DriveEta";
import PersonPinIcon from "@mui/icons-material/PersonPin";
import CategoryIcon from "@mui/icons-material/Category";
import EventNoteIcon from "@mui/icons-material/EventNote";
import WithRouter from "../../../hooks/WithRouter";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
  },
  list: { backgroundColor: theme.palette.primary.background },
  listSelected: {
    backgroundColor: theme.palette.primary.main,
  },
  listItemText: { color: theme.palette.primary.main },
  listItemIcon: { color: theme.palette.primary.main },
  listItemTextSelected: { color: theme.palette.primary.icons },
  listItemIconSelected: { color: theme.palette.primary.icons },
}));

function AdminDrawer(props) {
  const { router } = props;
  const classes = useStyles();
  const [openUsers, setOpenUsers] = React.useState(false);
  const [openTrips, setOpenTrips] = React.useState(false);
  const [openDrivers, setOpenDrivers] = React.useState(false);
  const handleClickUsers = () => {
    setOpenUsers(!openUsers);
  };
  const handleClickTrips = () => {
    setOpenTrips(!openTrips);
  };
  const handleClickDrivers = () => {
    setOpenDrivers(!openDrivers);
  };

  React.useEffect(() => {
    if (
      props.router.location.pathname.includes("users") &&
      !props.router.location.pathname.includes("trips")
    ) {
      setOpenUsers(true);
    }
    if (props.router.location.pathname.includes("trips")) {
      setOpenTrips(true);
    }
    if (
      (props.router.location.pathname.includes("drivers") &&
        !props.router.location.pathname.includes("trips")) ||
      (props.router.location.pathname.includes("events") &&
        !props.router.location.pathname.includes("trips"))
    ) {
      setOpenDrivers(true);
    }
  }, [props]);

  return (
    <Drawer
      variant="permanent"
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: drawerWidth,
          boxSizing: "border-box",
        },
      }}
    >
      <Toolbar />
      <Box sx={{ overflow: "auto" }}>
        <ListItem button key={"Users"} onClick={handleClickUsers}>
          <ListItemIcon>
            <GroupIcon className={classes.listItemIcon} />
          </ListItemIcon>
          <ListItemIcon>
            <ListItemText className={classes.listItemText}>Users</ListItemText>
          </ListItemIcon>
          {!openUsers ? <ExpandMore /> : <ExpandLess />}
        </ListItem>
        <Collapse in={openUsers} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("allusers")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/allusers"} className={classes.link}>
              <ListItem button key={"All Users"}>
                <ListItemIcon>
                  <AccountIcon
                    className={
                      !router.location.pathname.includes("allusers")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("allusers")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    All Users
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>
          </List>
        </Collapse>

        <Collapse in={openUsers} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("blockeduserslist")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/allblockeduserslist"} className={classes.link}>
              <ListItem button key={"Blocked Users"}>
                <ListItemIcon>
                  <BlockedIcon
                    className={
                      !router.location.pathname.includes("blockeduserslist")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("blockeduserslist")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    Blocked Users
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse>
        <Divider />
        <ListItem button key={"Trips"} onClick={handleClickTrips}>
          <ListItemIcon>
            <DriveEtaIcon className={classes.listItemIcon} />
          </ListItemIcon>
          <ListItemIcon>
            <ListItemText className={classes.listItemText}>Trips</ListItemText>
          </ListItemIcon>
          {!openTrips ? <ExpandMore /> : <ExpandLess />}
        </ListItem>
        <Collapse in={openTrips} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("alltrips")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/alltrips"} className={classes.link}>
              <ListItem button key={"Accepted Trips"}>
                <ListItemIcon>
                  <DriveEtaIcon
                    className={
                      !router.location.pathname.includes("alltrips")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("alltrips")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    Accepted Trips
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>
          </List>
        </Collapse>

        <Collapse in={openTrips} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("tripsbydriver")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/tripsbydriver"} className={classes.link}>
              <ListItem button key={"Trips By Driver"}>
                <ListItemIcon>
                  <PersonPinIcon
                    className={
                      !router.location.pathname.includes("tripsbydriver")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("tripsbydriver")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    Trips By Driver
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse>
        {/* <Collapse in={openTrips} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Link to={"/mainpage/tripsbypassenger"} className={classes.link}>
              <ListItem button key={"Trips By User"}>
                <ListItemIcon>
                  <GroupIcon className={classes.listItemIcon} />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText className={classes.listItemText}>
                    Trips By User
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse> */}
        <Collapse in={openTrips} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("categories")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link
              to={"/mainpage/dashboardtripscategories"}
              className={classes.link}
            >
              <ListItem button key={"Categories of trips"}>
                <ListItemIcon>
                  <CategoryIcon
                    className={
                      !router.location.pathname.includes("categories")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("categories")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    Categories of trips
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse>
        <Divider />
        <ListItem button key={"Drivers"} onClick={handleClickDrivers}>
          <ListItemIcon>
            <PersonPinIcon className={classes.listItemIcon} />
          </ListItemIcon>
          <ListItemIcon>
            <ListItemText className={classes.listItemText}>
              Drivers
            </ListItemText>
          </ListItemIcon>
          {!openDrivers ? <ExpandMore /> : <ExpandLess />}
        </ListItem>
        <Collapse in={openDrivers} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("alldrivers")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/alldrivers"} className={classes.link}>
              <ListItem button key={"All Drivers"}>
                <ListItemIcon>
                  <PersonPinIcon
                    className={
                      !router.location.pathname.includes("alldrivers")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("alldrivers")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    All Drivers
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>
          </List>
        </Collapse>

        <Collapse in={openDrivers} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("allevents")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link to={"/mainpage/dashboardallevents"} className={classes.link}>
              <ListItem button key={"All Events"}>
                <ListItemIcon>
                  <EventNoteIcon
                    className={
                      !router.location.pathname.includes("allevents")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("allevents")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    All Events
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse>

        <Collapse in={openDrivers} timeout="auto" unmountOnExit>
          <List
            component="div"
            disablePadding
            className={
              !router.location.pathname.includes("blockeddriverslist")
                ? classes.list
                : classes.listSelected
            }
          >
            <Link
              to={"/mainpage/allblockeddriverslist"}
              className={classes.link}
            >
              <ListItem button key={"Blocked Drivers"}>
                <ListItemIcon>
                  <BlockedIcon
                    className={
                      !router.location.pathname.includes("blockeddriverslist")
                        ? classes.listItemIcon
                        : classes.listItemIconSelected
                    }
                  />
                </ListItemIcon>
                <ListItemIcon>
                  <ListItemText
                    className={
                      !router.location.pathname.includes("blockeddriverslist")
                        ? classes.listItemText
                        : classes.listItemTextSelected
                    }
                  >
                    Blocked Drivers
                  </ListItemText>
                </ListItemIcon>
              </ListItem>
            </Link>{" "}
          </List>
        </Collapse>
      </Box>
    </Drawer>
  );
}

export default WithRouter(AdminDrawer);
