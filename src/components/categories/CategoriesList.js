import React, { Component } from "react";
import TableBody from "@mui/material/TableBody";
import CategoriesSummary from "./CategoriesSummary";

class CategoriesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAddCategory: false,
    };
  }

  render() {
    const { categories } = this.props;
    return (
      <TableBody>
        {categories &&
          categories.map((category) => (
            <CategoriesSummary category={category} />
          ))}
      </TableBody>
    );
  }
}

export default CategoriesList;
