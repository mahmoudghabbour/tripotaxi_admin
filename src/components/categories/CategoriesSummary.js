import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@mui/styles";
import Dialog from "@mui/material/Dialog";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import ConfirmDialog from "../drivers/ConfirmDialog";
import AddNewCategory from "../trips/AddNewCategory";
import { deleteCategory } from "../../store/actions/CategoriesAction";
import { Navigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  editIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
  deleteIcon: {
    color: theme.palette.primary.errColor,
    "&:hover": { cursor: "pointer" },
  },
}));

const CategoriesSummary = (props) => {
  const classes = useStyles();
  const { category, deleteCategory } = props;
  const [openEditCategory, setOpenEditCategory] = React.useState(false);
  const [openConfirmDialog, setOpenConfirmDialog] = React.useState(false);
  const handleOpenEditCategory = () => {
    setOpenEditCategory(true);
  };

  const handleCloseEditCategory = () => {
    setOpenEditCategory(false);
  };

  const handleOpenConfirmDialog = () => {
    setOpenConfirmDialog(true);
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };

  const handleDeleteCategory = () => {
    props.deleteCategory(category.id);
  };
  if (props.deleteCategoryStatus)
    return <Navigate to="/mainpage/dashboardtripscategories" />;
  else
    return (
      <TableRow
        key={category.id}
        sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
      >
        <Dialog
          open={openConfirmDialog}
          onClose={handleCloseConfirmDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <ConfirmDialog
            handleCloseConfirmDialog={handleCloseConfirmDialog}
            deleteCategory={props.deleteCategoryStatus}
            handleSubmit={handleDeleteCategory}
          />
        </Dialog>
        <Dialog
          open={openEditCategory}
          onClose={handleCloseEditCategory}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <AddNewCategory
            type="edit"
            category={category}
            handleClose={handleCloseEditCategory}
          />
        </Dialog>
        <TableCell component="th" scope="row">
          {category.categoryName}
        </TableCell>
        <TableCell align="center">{category.seatsNumber}</TableCell>
        <TableCell align="center">{category.minimumFareAtDay}</TableCell>
        <TableCell align="center">{category.minDay}</TableCell>
        <TableCell align="center">{category.kmDay}</TableCell>
        <TableCell align="center">{category.minimumFareAtNight}</TableCell>
        <TableCell align="center">{category.minNight}</TableCell>
        <TableCell align="center">{category.kmNight}</TableCell>
        <TableCell align="center">
          <EditIcon
            className={classes.editIcon}
            onClick={handleOpenEditCategory}
          />
          <DeleteIcon
            className={classes.deleteIcon}
            onClick={handleOpenConfirmDialog}
          />
        </TableCell>
      </TableRow>
    );
};

const mapStateToProps = (state, props) => {
  return {
    deleteCategoryStatus: state.categories.deleteCategory,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    deleteCategory: (categoryId) => dispatch(deleteCategory(categoryId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesSummary);
