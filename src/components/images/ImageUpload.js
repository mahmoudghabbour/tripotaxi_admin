import React, { Component } from "react";
import * as imageConversion from "image-conversion";
import { compose } from "redux";
import { withStyles } from "@mui/styles";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardMedia from "@mui/material/CardMedia";
import DeleteIcon from "@mui/icons-material/Delete";

const styles = (theme) => ({
  deleteIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
});

class ImageUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      base64: null,
    };
  }
  componentDidMount() {}

  // handleCompressImage = async (imageFile) => {
  //   imageConversion.compressAccurately(imageFile, 200).then((res) => {
  //     //The res in the promise is a compressed Blob type (which can be treated as a File type) file;
  //     const base64 = await this.handleConvertToBase64(res);
  //     this.setState({
  //       base64: base64,
  //     });
  //   });
  // };

  handleCheckImageSize = async (imageFile) => {
    // this.handleCompressImage(imageFile);
    var tempImageFile = null;
    var base64 = null;
    if (imageFile.size > 200 * 1024) {
      imageConversion.compressAccurately(imageFile, 200).then((res) => {
        //The res in the promise is a compressed Blob type (which can be treated as a File type) file;
        tempImageFile = res;
      });
      base64 = await this.handleConvertToBase64(tempImageFile);
      this.setState({
        base64: base64,
      });
      this.props.handleChangeImage(base64);
      // this.handleCompressImage(imageFile);
    } else {
      base64 = await this.handleConvertToBase64(imageFile);
      this.setState({
        base64: base64,
      });
      this.props.handleChangeImage(base64);
    }
  };

  handleUploadImage = async (e) => {
    var imageFile = e.target.files[0];
    this.handleCheckImageSize(imageFile);
  };

  handleConvertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  handleDeleteImage = () => {
    var base64 = "";
    this.setState({
      base64: base64,
    });
    this.props.handleChangeImage(base64);
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="flex-start"
        >
          {!this.state.base64 ? null : (
            <Card sx={{ maxWidth: 345 }}>
              <CardMedia
                component="img"
                height="140"
                image={this.state.base64}
                alt="green iguana"
              />
              <CardActions>
                <DeleteIcon
                  className={classes.deleteIcon}
                  onClick={this.handleDeleteImage}
                />
              </CardActions>
            </Card>
          )}
          {this.state.base64 ? null : (
            <Button variant="contained" color="primary" component="label">
              Upload Image
              <input
                type="file"
                style={{ display: "none" }}
                // onChange={this.onSelectMultipleFiles}
                onChange={this.handleUploadImage}
                accept="image/*"
                multiple
              />
            </Button>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

export default compose(withStyles(styles))(ImageUpload);
