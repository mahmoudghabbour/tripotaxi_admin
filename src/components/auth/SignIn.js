import React, { Component } from "react";
import { Navigate } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import TripoTaxiLogo from "../../svg/TripoTaxiLogo.svg";
import { signIn } from "../../store/actions/AuthActions";
import { CircularProgress } from "@mui/material";

const styles = (theme) => ({
  containerLogoGrid: {
    padding: theme.spacing(15, 0, 0, 0),
  },
  logo: {
    height: "20em",
    width: "20em",
  },
  circularProgress: {
    color: "#FFFFFF",
  },
});

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
    };
  }

  handleChange = (e, type) => {
    if (type === "email")
      this.setState({
        email: e.target.value,
      });
    if (type === "password")
      this.setState({
        password: e.target.value,
      });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signIn(this.state);
  };

  render() {
    const { classes, auth, authErrorSignIn, signInSpinner } = this.props;
    const token = localStorage.getItem("userToken");
    if (auth || token) return <Navigate to="/mainpage" />;
    else
      return (
        <React.Fragment>
          <Grid container component="main" sx={{ height: "100vh" }}>
            <CssBaseline />
            <Grid
              item
              xs={false}
              sm={4}
              md={7}
              sx={{
                backgroundImage: TripoTaxiLogo,
                backgroundRepeat: "no-repeat",
                backgroundColor: (t) => t.palette.primary.main,
                backgroundSize: "cover",
                backgroundPosition: "center",
              }}
            >
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
                className={classes.containerLogoGrid}
              >
                <Grid item>
                  <img
                    src={TripoTaxiLogo}
                    align="center"
                    alt="app logo"
                    className={classes.logo}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              sm={8}
              md={5}
              component={Paper}
              elevation={6}
              square
            >
              <Box
                sx={{
                  my: 8,
                  mx: 4,
                  marginTop: 15,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Avatar
                  sx={{ m: 1, bgcolor: (theme) => theme.palette.primary.main }}
                >
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                  Sign in
                </Typography>
                <Box
                  component="form"
                  noValidate
                  // onSubmit={handleSubmit}
                  sx={{ mt: 1 }}
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    onChange={(e) => this.handleChange(e, "email")}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={(e) => this.handleChange(e, "password")}
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{
                      mt: 3,
                      mb: 2,
                      color: (theme) => theme.palette.primary.icons,
                    }}
                    onClick={this.handleSubmit}
                  >
                    {signInSpinner === "loading" ? (
                      <CircularProgress size={25} sx={{ color: "white" }} />
                    ) : (
                      "Sign In"
                    )}
                  </Button>
                  <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Typography
                      variant="subtitle2"
                      sx={{ color: (theme) => theme.palette.primary.errColor }}
                    >
                      {authErrorSignIn}
                    </Typography>
                  </Grid>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </React.Fragment>
      );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.auth.auth,
    signInSpinner: state.auth.signInSpinner,
    authErrorSignIn: state.auth.authErrorSignIn,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    signIn: (cred) => dispatch(signIn(cred)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SignIn);
