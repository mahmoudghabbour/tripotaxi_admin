import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import ImageUpload from "../images/ImageUpload";

const styles = (theme) => ({
  textField: {
    width: "100%",
  },
});

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.user ? props.user.name : null,
      email: props.user ? props.user.email : null,
      gender: props.user ? props.user.gender : null,
      phone: props.user ? props.user.phone : null,
      imageProfile: props.user ? props.user.imageProfile : null,
      id: props.user ? props.user.id : null,
    };
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeImage = (imageProfile) => {
    this.setState({
      imageProfile: imageProfile,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{ color: (theme) => theme.palette.primary.main }}
        >
          {"Edit User"}
        </DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <ImageUpload
              imageProfile={this.state.imageProfile}
              handleChangeImage={this.handleChangeImage}
            />
          </Grid>
          <TextField
            required
            id="filled-required"
            label="Name"
            variant="filled"
            onChange={this.handleChange}
            value={this.state.name}
            className={classes.textField}
          />
          <TextField
            required
            id="filled-required"
            label="E-mail"
            variant="filled"
            onChange={this.handleChange}
            value={this.state.email}
            className={classes.textField}
          />
          <TextField
            id="standard-select-currency"
            select
            label="Gender"
            variant="standard"
            value={this.state.gender}
            onChange={this.handleChange}
            className={classes.textField}
          >
            <MenuItem value={"MALE"}>Male</MenuItem>
            <MenuItem value={"FEMALE"}>Female</MenuItem>
          </TextField>
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" color="primary">
            Edit
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(EditUser);
