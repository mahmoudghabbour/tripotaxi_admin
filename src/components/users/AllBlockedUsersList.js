import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import Dialog from "@mui/material/Dialog";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ConfirmDialog from "../drivers/ConfirmDialog";
import { blockOrUnBlockUser } from "../../store/actions/UsersAction";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  visibilityIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
});

class AllBlockedUsersList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleOpenProfile = (userId) => {
    window.open("/adminuserprofile/" + userId);
  };

  handleOpenConfirmDialog = () => {
    this.setState({
      openConfirmDialog: true,
    });
  };

  handleCloseConfirmDialog = () => {
    this.setState({
      openConfirmDialog: false,
    });
  };

  handleUnBlock = () => {
    const updateUser = { ...this.props.user, isAccountBlocked: false };
    this.props.blockOrUnBlockUser(updateUser);
  };

  render() {
    const { classes, user, unBlockUserStatus } = this.props;
    if (unBlockUserStatus === "success")
      return <Navigate to="/mainpage/allusers" />;
    return (
      <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
        <Dialog
          open={this.state.openConfirmDialog}
          onClose={this.handleCloseConfirmDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <ConfirmDialog
            handleCloseConfirmDialog={this.handleCloseConfirmDialog}
            handleSubmit={this.handleUnBlock}
          />
        </Dialog>
        <TableCell component="th" scope="row">
          {user.name}
        </TableCell>
        <TableCell align="center">{user.email}</TableCell>
        <TableCell align="center">{user.phone}</TableCell>
        <TableCell align="center">{user.id}</TableCell>
        <TableCell>
          <Button
            variant="contained"
            sx={{
              color: (theme) => theme.palette.primary.icons,
            }}
            onClick={this.handleOpenConfirmDialog}
          >
            Un Block
          </Button>
        </TableCell>
        <TableCell align="center">
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={() => this.handleOpenProfile(user.id)}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    unBlockUserStatus: state.users.blockUser,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    blockOrUnBlockUser: (user) => dispatch(blockOrUnBlockUser(user)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllBlockedUsersList);
