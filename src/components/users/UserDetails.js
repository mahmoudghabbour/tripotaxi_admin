import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import TableCell from "@mui/material/TableCell";
import Dialog from "@mui/material/Dialog";
import EditIcon from "@mui/icons-material/Edit";
import BlockIcon from "@mui/icons-material/Block";
import DeleteIcon from "@mui/icons-material/Delete";
import { FaStar } from "react-icons/fa";
import {
  deleteUser,
  getOneUser,
  blockOrUnBlockUser,
} from "../../store/actions/UsersAction";
import withRouter from "../../hooks/WithRouter";
import EditUser from "./EditUser";
import ConfirmDialog from "../drivers/ConfirmDialog";

const styles = (theme) => ({
  star: {
    cursor: "pointer",
    transition: "color 2000ms",
  },
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "100%",
  },
});

class UserDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: null,
      openEditUser: false,
      openConfirmDialog: false,
    };
  }
  componentDidMount() {
    this.props.getOneUser(this.props.router.params.id);
  }

  handleOpenEditUser = () => {
    this.setState({
      openEditUser: true,
    });
  };

  handleCloseEditUser = () => {
    this.setState({
      openEditUser: false,
    });
  };

  handleOpenConfirmDialog = (type) => {
    if (type === "block")
      this.setState({
        type: "block",
        openConfirmDialog: true,
      });
    else
      this.setState({
        type: "delete",
        openConfirmDialog: true,
      });
  };

  handleCloseConfirmDialog = () => {
    this.setState({
      openConfirmDialog: false,
    });
  };

  handleBlockUser = () => {
    if (this.state.type === "block") {
      const updateUser = { ...this.props.user, isAccountBlocked: true };
      this.props.blockOrUnBlockUser(updateUser);
    } else this.props.deleteUser(this.props.user.id);
  };

  render() {
    const { classes, user, blockUserStatus, deleteUserStatus } = this.props;
    const average = 5;
    if (blockUserStatus === "success" || deleteUserStatus === "success")
      return <Navigate to="/mainpage/allusers" />;
    return (
      <React.Fragment>
        <Dialog
          open={this.state.openEditUser}
          onClose={this.handleCloseEditUser}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <EditUser user={user} />
        </Dialog>
        <Dialog
          open={this.state.openConfirmDialog}
          onClose={this.handleCloseConfirmDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <ConfirmDialog
            handleCloseConfirmDialog={this.handleCloseConfirmDialog}
            handleSubmit={this.handleBlockUser}
          />
        </Dialog>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          style={{ marginTop: "5em" }}
        >
          <Grid item>
            <Avatar alt="Remy Sharp" src="" sx={{ width: 150, height: 150 }} />
          </Grid>
          <Grid item>
            <Typography variant="h2">
              {user ? (user.name ? user.name : null) : null}
            </Typography>
          </Grid>
          <Grid item>
            {[...Array(5)].map((star, i) => {
              const ratingValue = i + 1;
              return (
                <label>
                  {" "}
                  <FaStar
                    className={classes.star}
                    color={ratingValue <= average ? "#2DBB54" : "#e4e5e9"}
                    size={25}
                  />
                </label>
              );
            })}
          </Grid>
          <Grid item>
            <Grid
              container
              direction="row"
              justifyContent="space-around"
              alignItems="center"
            >
              <Button
                color="primary"
                variant="contained"
                startIcon={<EditIcon />}
                onClick={this.handleOpenEditUser}
              >
                Edit
              </Button>
              <Button
                variant="contained"
                startIcon={<BlockIcon />}
                sx={{
                  marginInlineStart: "3em",
                  backgroundColor: (theme) => theme.palette.primary.light,
                }}
                onClick={() => this.handleOpenConfirmDialog("block")}
              >
                Block
              </Button>
              <Button
                variant="contained"
                startIcon={<DeleteIcon />}
                sx={{
                  marginInlineStart: "3em",
                  color: (theme) => theme.palette.primary.icons,
                  backgroundColor: (theme) => theme.palette.primary.errColor,
                  "&:hover": {
                    color: (theme) => theme.palette.primary.icons,
                    backgroundColor: (theme) => theme.palette.primary.errColor,
                  },
                }}
                onClick={() => this.handleOpenConfirmDialog("delete")}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <TableHead style={{ width: "100%" }}>
          <TableRow className={classes.tableRow}>
            <Typography variant="h5">Personal Information</Typography>
          </TableRow>
        </TableHead>
        <Divider style={{ width: "100%" }} />
        <TableBody>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Name: {user ? (user.name ? user.name : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                E-mail: {user ? (user.email ? user.email : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Nationality Number:{" "}
                {user ? (user.nationality ? user.nationality : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Phone Number: {user ? (user.phone ? user.phone : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Gender: {user ? (user.gender ? user.gender : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Certificate Number:{" "}
                {user
                  ? user.driverCertificateNumber
                    ? user.driverCertificateNumber
                    : null
                  : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableBody>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    user: state.users.oneUser,
    blockUserStatus: state.users.blockUser,
    deleteUserStatus: state.users.deleteUser,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getOneUser: (userId) => dispatch(getOneUser(userId)),
    blockOrUnBlockUser: (userId) => dispatch(blockOrUnBlockUser(userId)),
    deleteUser: (user) => dispatch(deleteUser(user)),
  };
};

export default withRouter(
  compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps)
  )(UserDetails)
);
