import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
// import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import VisibilityIcon from "@mui/icons-material/Visibility";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  visibilityIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
});

class AllUsersList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleOpenProfile = (userId) => {
    window.open("/adminuserprofile/" + userId);
  };
  render() {
    const { classes, user } = this.props;
    return (
      <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
        <TableCell component="th" scope="row">
          {user.name}
        </TableCell>
        <TableCell align="center">{user.email}</TableCell>
        <TableCell align="center">{user.phone}</TableCell>
        <TableCell align="center">{user.id}</TableCell>
        <TableCell align="center">
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={() => this.handleOpenProfile(user.id)}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllUsersList);
