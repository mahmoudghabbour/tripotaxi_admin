import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import { MenuItem } from "@mui/material";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import { getTripsByPassenger } from "../../store/actions/TripsActions";
import { getAllUsers } from "../../store/actions/UsersAction";
import AllPassengerTrips from "../trips/AllPassengerTrips";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "20em",
    backgroundColor: theme.palette.primary.background,
  },
  typography: {
    color: theme.palette.primary.main,
  },
});

class DashboardTripsByPassenger extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passenger: null,
      passengerName: "",
    };
  }
  componentDidMount() {
    this.props.getAllUsers();
  }

  handleChangePassenger = (event) => {
    for (let i = 0; i < this.props.allPassengers.length; i++)
      if (event.target.value === this.props.allPassengers[i].name) {
        this.setState({
          driver: this.props.allPassengers[i],
          driverName: event.target.value,
        });
        this.props.getTripsByPassenger(this.props.allPassengers[i].id);
      }
  };
  render() {
    const { classes, allPassengers, passengerTrips } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <React.Fragment>
          <TableContainer sx={{ display: "flex", marginTop: "5em" }}>
            <AdminDrawer />
            <Box sx={{ mt: 5 }}>
              <TextField
                id="outlined-select-currency"
                select
                label="Select"
                value={this.state.driverName}
                onChange={(e) => this.handleChangePassenger(e)}
                sx={{ width: "100%" }}
              >
                {allPassengers &&
                  allPassengers.map((passenger) => (
                    <MenuItem key={passenger.id} value={passenger.name}>
                      {passenger.name}
                    </MenuItem>
                  ))}
              </TextField>

              <Table sx={{ flexGrow: 1, p: 0 }}>
                <TableHead style={{ width: "100%" }}>
                  <TableRow className={classes.tableRow}>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Driver Name
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Passenger Name
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Trip ID
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Created At
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Status
                      </Typography>
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <Typography variant="h6" className={classes.typography}>
                        Details
                      </Typography>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ width: "100%" }}>
                  {passengerTrips &&
                    passengerTrips.map((trip) => {
                      return <AllPassengerTrips key={trip.id} trip={trip} />;
                    })}
                </TableBody>
              </Table>
            </Box>
          </TableContainer>
        </React.Fragment>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    allPassengers: state.users.users,
    passengerTrips: state.trips.passengerTrips,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllUsers: () => dispatch(getAllUsers()),
    getTripsByPassenger: (passengerId) =>
      dispatch(getTripsByPassenger(passengerId)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardTripsByPassenger);
