import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
import CssBaseline from "@mui/material/CssBaseline";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import SearchIcon from "@mui/icons-material/Search";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import TripsByDriverList from "../drivers/TripsByDriverList";
import {
  getTripsByDriver,
  getFilteredCompletedTrips,
} from "../../store/actions/TripsActions";
import { getAllDrivers } from "../../store/actions/DriversAction";
import { getAllCategories } from "../../store/actions/CategoriesAction";
// import AllTripsList from "../trips/AllTripsList";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "20em",
    backgroundColor: theme.palette.primary.background,
  },
  typography: {
    color: theme.palette.primary.main,
  },
  searchButton: {
    width: "10em",
  },
});

class DashboardTripsByDriver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateValue: new Date(),
      timeValue: new Date(),
      driver: null,
      driverId: null,
      driverName: "",
      category: null,
      categoryName: "",
    };
  }
  componentDidMount() {
    this.props.getAllDrivers();
    this.props.getAllCategories();
  }

  handleChangeDriver = (event) => {
    for (let i = 0; i < this.props.allDrivers.length; i++)
      if (event.target.value === this.props.allDrivers[i].name) {
        this.setState({
          driver: this.props.allDrivers[i],
          driverId: this.props.allDrivers[i].id,
          driverName: event.target.value,
        });
        // this.props.getTripsByDriver(this.props.allDrivers[i].id);
      }
  };

  handleChangeCategory = (event) => {
    for (let i = 0; i < this.props.categories.length; i++)
      if (event.target.value === this.props.categories[i].categoryName) {
        this.setState({
          category: this.props.categories[i],
          categoryName: event.target.value,
        });
        // this.props.getTripsByDriver(this.props.allDrivers[i].id);
      }
  };

  handleChangeDate = (newValue) => {
    this.setState({
      dateValue: newValue,
    });
  };

  handleChangeTime = (newValue) => {
    this.setState({
      timeValue: newValue,
    });
  };

  handleSubmit = () => {
    var info = {
      driverName: this.state.driverName ? this.state.driverName : null,
      driverId: this.state.driverId ? this.state.driverId : null,
      categoryName: this.state.categoryName ? this.state.categoryName : null,
    };
    this.props.getFilteredCompletedTrips(info);
  };

  render() {
    const { classes, allDrivers, categories, driverTrips } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <AdminDrawer />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            >
              <Grid item xs={6}>
                <TextField
                  id="outlined-select-currency"
                  select
                  label="Driver Name"
                  value={this.state.driverName}
                  onChange={(e) => this.handleChangeDriver(e)}
                  sx={{ width: "100%" }}
                >
                  {allDrivers &&
                    allDrivers.map((driver) => (
                      <MenuItem key={driver.id} value={driver.name}>
                        {driver.name}
                      </MenuItem>
                    ))}
                </TextField>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-select-currency"
                  select
                  label="Select Trip Type"
                  value={this.state.categoryName}
                  onChange={(e) => this.handleChangeCategory(e)}
                  sx={{ width: "100%" }}
                >
                  {categories &&
                    categories.map((category) => (
                      <MenuItem key={category.id} value={category.categoryName}>
                        {category.categoryName}
                      </MenuItem>
                    ))}
                </TextField>
              </Grid>
              <Grid item xs={6}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <Stack spacing={3}>
                    <DatePicker
                      label="Trip Date"
                      openTo="year"
                      views={["year", "month", "day"]}
                      value={this.state.dateValue}
                      onChange={(newValue) => {
                        this.handleChangeDate(newValue);
                      }}
                      renderInput={(params) => <TextField {...params} />}
                    />
                  </Stack>
                </LocalizationProvider>
              </Grid>
              <Grid item xs={6}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <Stack spacing={3}>
                    <TimePicker
                      value={this.state.timeValue}
                      onChange={(newValue) => {
                        this.handleChangeTime(newValue);
                      }}
                      renderInput={(params) => <TextField {...params} />}
                    />
                  </Stack>
                </LocalizationProvider>
              </Grid>
            </Grid>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
              sx={{ padding: 3 }}
            >
              <Button
                variant="contained"
                startIcon={<SearchIcon />}
                onClick={this.handleSubmit}
                className={classes.searchButton}
              >
                Search
              </Button>
            </Grid>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Driver Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Customer Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Start Location
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Destination Location
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Trip Date
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Trip Type
                    </TableCell>
                    {/* <TableCell align="center">Trip Fare</TableCell> */}
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Trip Status
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      View Trip
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {driverTrips &&
                    driverTrips.map((trip) => (
                      <TripsByDriverList trip={trip} />
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    categories: state.categories.categories,
    allDrivers: state.drivers.drivers,
    driverTrips: state.trips.driverTrips,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllCategories: () => dispatch(getAllCategories()),
    getAllDrivers: () => dispatch(getAllDrivers()),
    getTripsByDriver: (driverId) => dispatch(getTripsByDriver(driverId)),
    getFilteredCompletedTrips: (info) =>
      dispatch(getFilteredCompletedTrips(info)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardTripsByDriver);
