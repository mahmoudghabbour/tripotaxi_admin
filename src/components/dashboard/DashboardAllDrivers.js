import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Paper from "@mui/material/Paper";
import Dialog from "@mui/material/Dialog";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import AllDriversList from "../drivers/AllDriversList";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import EditDriver from "../drivers/EditDriver";
import { getAllDrivers } from "../../store/actions/DriversAction";
const styles = (theme) => ({
  tableTitle: {
    color: theme.palette.primary.main,
  },
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "20em",
    backgroundColor: theme.palette.primary.background,
  },
  typography: {
    color: theme.palette.primary.main,
  },
  fab: {
    margin: 0,
    top: 290,
    right: 0,
    bottom: 20,
    left: 1010,
    position: "fixed",
    zIndex: 2,
  },
});

class DashboardAllDrivers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openEditDriver: false,
    };
  }
  componentDidMount() {
    this.props.getAllDrivers();
  }

  handleOpenEditDriver = () => {
    this.setState({
      openEditDriver: true,
    });
  };

  handleCloseEditDriver = () => {
    this.setState({
      openEditDriver: false,
    });
  };

  render() {
    const { classes, drivers } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <Box sx={{ display: "flex" }}>
          <Dialog
            open={this.state.openEditDriver}
            onClose={this.handleCloseEditDriver}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            fullWidth={true}
          >
            <EditDriver type="add" />
          </Dialog>
          <AdminDrawer
            handleOpenDashboardDrivers={this.handleOpenDashboardDrivers}
          />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={6}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Drivers
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      E-mail
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Nationality Number
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Phone
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      ID
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      View
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {drivers &&
                    drivers.map((driver) => {
                      return <AllDriversList key={driver.id} driver={driver} />;
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <Fab
              aria-label="add"
              className={classes.fab}
              color="primary"
              onClick={this.handleOpenEditDriver}
            >
              <AddIcon />
            </Fab>
          </Box>
        </Box>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    drivers: state.drivers.drivers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllDrivers: () => dispatch(getAllDrivers()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardAllDrivers);
