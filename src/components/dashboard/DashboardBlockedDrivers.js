import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import { getAllBlockedDrivers } from "../../store/actions/DriversAction";
import AllBlockedDriversList from "../drivers/AllBlockedDriversList";

const styles = (theme) => ({
  tableTitle: {
    color: theme.palette.primary.main,
  },
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "20em",
    backgroundColor: theme.palette.primary.background,
  },
  typography: {
    color: theme.palette.primary.main,
  },
});

class DashboardBlockedDrivers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.getAllBlockedDrivers();
  }
  render() {
    const { classes, drivers } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <Box sx={{ display: "flex" }}>
          <AdminDrawer
            handleOpenDashboardDrivers={this.handleOpenDashboardDrivers}
          />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={7}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Blocked Drivers
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      E-mail
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Nationality Number
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Phone
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      ID
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Un Block
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      View
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {drivers &&
                    drivers.map((driver) => {
                      return (
                        <AllBlockedDriversList
                          key={driver.id}
                          driver={driver}
                        />
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    drivers: state.drivers.drivers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllBlockedDrivers: () => dispatch(getAllBlockedDrivers()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardBlockedDrivers);
