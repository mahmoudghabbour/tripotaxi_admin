import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@mui/material/Grid";
import Dialog from "@mui/material/Dialog";
import Button from "@mui/material/Button";
import Table from "@mui/material/Table";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import TableRow from "@mui/material/TableRow";
import CssBaseline from "@mui/material/CssBaseline";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import AddNewCategory from "../trips/AddNewCategory";
import { getAllCategories } from "../../store/actions/CategoriesAction";
import CategoriesList from "../categories/CategoriesList";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

class DashboardTripsCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAddCategory: false,
      openSnackbar: false,
    };
  }

  componentDidMount() {
    this.props.getAllCategories();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.addSuccessfully) this.props.getAllCategories();
    if (nextProps.addSuccessfully) this.handleOpenSnackbar();
  }

  handleOpenAddCategory = () => {
    this.setState({
      openAddCategory: true,
    });
  };

  handleCloseAddCategory = () => {
    this.setState({
      openAddCategory: false,
    });
  };

  handleOpenSnackbar = () => {
    this.setState({
      openSnackbar: true,
    });
  };

  handleCloseSnackbar = () => {
    this.setState({
      openSnackbar: false,
    });
  };

  render() {
    const { categories } = this.props;
    return (
      <Box sx={{ display: "flex" }}>
        <Snackbar
          open={this.state.openSnackbar}
          autoHideDuration={6000}
          onClose={this.handleCloseSnackbar}
        >
          <Alert severity="success">Category added successfully</Alert>
        </Snackbar>
        <Dialog
          open={this.state.openAddCategory}
          onClose={this.handleCloseAddCategory}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <AddNewCategory
            type="add"
            handleClose={this.handleCloseAddCategory}
          />
        </Dialog>
        <CssBaseline />
        <AdminDrawer />
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
          <Toolbar />
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="flex-end"
          >
            <Button
              variant="contained"
              sx={{ marginTop: "3em", marginInlineEnd: "0.5em" }}
              onClick={this.handleOpenAddCategory}
            >
              Add new category
            </Button>

            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }}>
                <TableHead>
                  <TableRow>
                    <TableCell
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Category Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Seats number
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Minimum fare&nbsp;(day)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      SYP/min&nbsp;(day)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      SYP/km&nbsp;(day)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Minimum fare&nbsp;(night)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      SYP/min&nbsp;(night)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      SYP/km&nbsp;(night)
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Operations
                    </TableCell>
                  </TableRow>
                </TableHead>
                <CategoriesList categories={categories} />
              </Table>
            </TableContainer>
          </Grid>
        </Box>
      </Box>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.auth,
    categories: state.categories.categories,
    addSuccessfully: state.categories.addSuccessfully,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllCategories: () => dispatch(getAllCategories()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardTripsCategories);
