import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import CircularProgress from "@mui/material/CircularProgress";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import { getAllEvents } from "../../store/actions/EventsAction";
import EventsList from "../events/EventsList";

const styles = (theme) => ({});

class DashboardAllEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allEvents: null,
    };
  }
  componentDidMount() {
    this.props.getAllEvents();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.allEvents)
      this.setState({
        allEvents: nextProps.allEvents,
      });
  }
  render() {
    return (
      <Box sx={{ display: "flex" }}>
        <AdminDrawer
          handleOpenDashboardDrivers={this.handleOpenDashboardDrivers}
        />
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
          <Toolbar />
          {this.state.allEvents ? (
            <Grid
              container
              direction="row"
              justifyContent="space-evenly"
              alignItems="center"
            >
              {this.state.allEvents &&
                this.state.allEvents.map((event) => {
                  return <EventsList event={event} />;
                })}
            </Grid>
          ) : (
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
              sx={{ minHeight: "30em" }}
            >
              <Grid item>
                <CircularProgress />
              </Grid>
            </Grid>
          )}
        </Box>
      </Box>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    allEvents: state.events.allEvents,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllEvents: () => dispatch(getAllEvents()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardAllEvents);
