import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import AdminDrawer from "../layout/drawers/AdminDrawer";
import { getAllAcceptedTrips } from "../../store/actions/TripsActions";
import AllTripsList from "../trips/AllTripsList";

const styles = (theme) => ({
  tableTitle: {
    color: theme.palette.primary.main,
  },
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "20em",
    backgroundColor: theme.palette.primary.background,
  },
  typography: {
    color: theme.palette.primary.main,
  },
});

class DashboardAllTrips extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.getAllAcceptedTrips();
  }
  render() {
    const { classes, driverTrips, acceptedTrips } = this.props;
    if (localStorage.getItem("userToken"))
      return (
        <Box sx={{ display: "flex" }}>
          <AdminDrawer
            handleOpenDashboardDrivers={this.handleOpenDashboardDrivers}
          />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <TableContainer component={Paper} sx={{ marginTop: "1em" }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" colSpan={6}>
                      <Typography variant="h5" className={classes.tableTitle}>
                        Accepted Trips
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Driver Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Passenger Name
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Trip ID
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Created At
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Status
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        fontWeight: "bold",
                        color: (theme) => theme.palette.primary.main,
                      }}
                    >
                      Details
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {acceptedTrips &&
                    acceptedTrips.map((trip) => {
                      return <AllTripsList key={trip.id} trip={trip} />;
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      );
    else return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    driverTrips: state.trips.driverTrips,
    acceptedTrips: state.trips.acceptedTrips,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllAcceptedTrips: () => dispatch(getAllAcceptedTrips()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardAllTrips);
