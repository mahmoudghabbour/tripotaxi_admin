import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import Dialog from "@mui/material/Dialog";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ConfirmDialog from "./ConfirmDialog";
import { blockOrUnBlockDriver } from "../../store/actions/DriversAction";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
});

class AllBlockedDriversList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openConfirmDialog: false,
    };
  }
  handleOpenProfile = (driverId) => {
    window.open("/admindriverprofile/" + driverId);
  };

  handleOpenConfirmDialog = () => {
    this.setState({
      openConfirmDialog: true,
    });
  };

  handleCloseConfirmDialog = () => {
    this.setState({
      openConfirmDialog: false,
    });
  };

  handleUnBlock = () => {
    const updateDriver = { ...this.props.driver, isAccountBlocked: false };
    this.props.blockOrUnBlockDriver(updateDriver);
  };

  render() {
    const { classes, driver, unBlockDriverStatus } = this.props;
    if (unBlockDriverStatus === "success")
      return <Navigate to="/mainpage/alldrivers" />;
    return (
      <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
        <Dialog
          open={this.state.openConfirmDialog}
          onClose={this.handleCloseConfirmDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <ConfirmDialog
            handleCloseConfirmDialog={this.handleCloseConfirmDialog}
            handleSubmit={this.handleUnBlock}
          />
        </Dialog>
        <TableCell component="th" scope="row">
          {driver.name}
        </TableCell>
        <TableCell align="center">{driver.email}</TableCell>
        <TableCell align="center">{driver.nationality}</TableCell>
        <TableCell align="center">{driver.phone}</TableCell>
        <TableCell align="center">{driver.id}</TableCell>
        <TableCell align="center">
          <Button
            variant="contained"
            sx={{
              color: (theme) => theme.palette.primary.icons,
            }}
            onClick={this.handleOpenConfirmDialog}
          >
            Un Block
          </Button>
        </TableCell>
        <TableCell align="center">
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={() => this.handleOpenProfile(driver.id)}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    unBlockDriverStatus: state.drivers.blockDriver,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    blockOrUnBlockDriver: (driver) => dispatch(blockOrUnBlockDriver(driver)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllBlockedDriversList);
