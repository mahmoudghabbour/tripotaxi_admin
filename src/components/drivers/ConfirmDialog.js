import React from "react";
import { makeStyles } from "@mui/styles";
import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import { CircularProgress } from "@mui/material";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  button: {
    width: "100%",
  },
  wrapper: {
    width: "100%",
    margin: theme.spacing(1),
    position: "relative",
  },
}));

function ConfirmDialog(props) {
  const classes = useStyles();
  const Navigate = useNavigate();
  if (props.deleteCategoryStatus === "success")
    return <Navigate to="/mainpage/dashboardtripscategories" />;
  else
    return (
      <React.Fragment>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{ color: (theme) => theme.palette.primary.main }}
        >
          {"Are you sure?"}
        </DialogTitle>
        <DialogActions>
          <div className={classes.wrapper}>
            {props.deleteCategory && props.deleteCategory === "loading" ? (
              <Button className={classes.button} onClick={props.handleSubmit}>
                <CircularProgress />
              </Button>
            ) : (
              <Button className={classes.button} onClick={props.handleSubmit}>
                Yes
              </Button>
            )}
          </div>
          <Button
            className={classes.button}
            onClick={props.handleCloseConfirmDialog}
          >
            No
          </Button>
        </DialogActions>
      </React.Fragment>
    );
}

export default ConfirmDialog;
