import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import { styled } from "@mui/material/styles";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import TableCell from "@mui/material/TableCell";
import Dialog from "@mui/material/Dialog";
import CircularProgress from "@mui/material/CircularProgress";
import TimelineIcon from "@mui/icons-material/Timeline";
import EditIcon from "@mui/icons-material/Edit";
import BlockIcon from "@mui/icons-material/Block";
import DeleteIcon from "@mui/icons-material/Delete";
import { FaStar } from "react-icons/fa";
import {
  getOneDriver,
  blockOrUnBlockDriver,
  deleteDriver,
} from "../../store/actions/DriversAction";
import withRouter from "../../hooks/WithRouter";
import EditDriver from "./EditDriver";
import ConfirmDialog from "./ConfirmDialog";
import {
  getRateForDriver,
  getDriverCar,
  getHistoryTracking,
  getDriverWallet,
  collectMoney,
  clearPaidCostStatus,
} from "../../store/actions/DriversAction";
import {
  getTodayDriverInvoices,
  getLastWeekDriverInvoices,
  getDriverInvoicesThisMonth,
  getDriverInvoicesLastMonth,
  getPaidCost,
} from "../../store/actions/DriverInvoicesAction";
import DriverMapComponent from "../map/DriverMapComponent";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(0),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const styles = (theme) => ({
  star: {
    cursor: "pointer",
    transition: "color 2000ms",
  },
  tableRow: {
    width: "100%",
  },
  tableCell: {
    width: "100%",
  },
  textField: {
    height: "50%",
    width: "50%",
  },
  containerGrid: {
    padding: theme.spacing(2),
  },
});

class DriverDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: null,
      payAmount: null,
      isFirstTime: false,
      openMapDialog: false,
      openEditDriver: false,
      openConfirmDialog: false,
      companyRatio: 20,
      driverRatio: 80,
    };
  }
  componentDidMount() {
    this.props.getOneDriver(this.props.router.params.id);
    this.props.getDriverCar(this.props.router.params.id);
    this.props.getRateForDriver(this.props.router.params.id);
    this.props.getTodayDriverInvoices(this.props.router.params.id);
    this.props.getLastWeekDriverInvoices(this.props.router.params.id);
    this.props.getDriverInvoicesThisMonth(this.props.router.params.id);
    this.props.getDriverInvoicesLastMonth(this.props.router.params.id);
    this.props.getPaidCost(this.props.router.params.id);
    this.props.getDriverWallet(this.props.router.params.id);
    this.props.getHistoryTracking();
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.invoicesLastMonth && !this.state.isFirstTime) {
      this.setState({
        isFirstTime: true,
      });
      // Revenue
      var invoicesToday = nextProps.invoicesToday;
      var invoicesLastWeek = nextProps.invoicesLastWeek;
      var invoicesThisMonth = nextProps.invoicesThisMonth;
      var invoicesLastMonth = nextProps.invoicesLastMonth;
      // Company Net Revenue
      var invoicesTodayCompany =
        (nextProps.invoicesToday * this.state.companyRatio) / 100;
      var invoicesLastWeekCompany =
        (nextProps.invoicesLastWeek * this.state.companyRatio) / 100;
      var invoicesThisMonthCompany =
        (nextProps.invoicesThisMonth * this.state.companyRatio) / 100;
      var invoicesLastMonthCompany =
        (nextProps.invoicesLastMonth * this.state.companyRatio) / 100;
      // Driver Net Revenue
      var invoicesTodayDriver =
        (nextProps.invoicesToday * this.state.driverRatio) / 100;
      var invoicesLastWeekDriver =
        (nextProps.invoicesLastWeek * this.state.driverRatio) / 100;
      var invoicesThisMonthDriver =
        (nextProps.invoicesThisMonth * this.state.driverRatio) / 100;
      var invoicesLastMonthDriver =
        (nextProps.invoicesLastMonth * this.state.driverRatio) / 100;

      this.setState({
        invoicesToday: invoicesToday,
        invoicesLastWeek: invoicesLastWeek,
        invoicesThisMonth: invoicesThisMonth,
        invoicesLastMonth: invoicesLastMonth,
        invoicesTodayCompany: invoicesTodayCompany,
        invoicesLastWeekCompany: invoicesLastWeekCompany,
        invoicesThisMonthCompany: invoicesThisMonthCompany,
        invoicesLastMonthCompany: invoicesLastMonthCompany,
        invoicesTodayDriver: invoicesTodayDriver,
        invoicesLastWeekDriver: invoicesLastWeekDriver,
        invoicesThisMonthDriver: invoicesThisMonthDriver,
        invoicesLastMonthDriver: invoicesLastMonthDriver,
      });
    }
    if (nextProps.paidCostStatus === "success") {
      this.setState({
        payAmount: null,
      });
      this.props.getDriverWallet(this.props.router.params.id);
      this.props.clearPaidCostStatus();
    }
  }

  handleOpenEditDriver = () => {
    this.setState({
      openEditDriver: true,
    });
  };

  handleCloseEditDriver = () => {
    this.setState({
      openEditDriver: false,
    });
  };

  handleOpenMapDialog = () => {
    this.setState({
      openMapDialog: true,
    });
  };

  handleCloseMapDialog = () => {
    this.setState({
      openMapDialog: false,
    });
  };

  handleOpenConfirmDialog = (type) => {
    if (type === "block")
      this.setState({
        type: "block",
        openConfirmDialog: true,
      });
    else
      this.setState({
        type: "delete",
        openConfirmDialog: true,
      });
  };

  handleCloseConfirmDialog = () => {
    this.setState({
      openConfirmDialog: false,
    });
  };

  handleChange = (event) => {
    this.setState({
      payAmount: event.target.value,
    });
  };

  handlePay = (id) => {
    this.props.collectMoney(this.state.payAmount, id);
  };

  handleBlockDriver = () => {
    if (this.state.type === "block") {
      const updateDriver = { ...this.props.driver, isAccountBlocked: true };
      this.props.blockOrUnBlockDriver(updateDriver);
    } else this.props.deleteDriver(this.props.driver.id);
  };

  render() {
    const {
      classes,
      driver,
      blockDriverStatus,
      driverRate,
      driverCar,
      deleteDriverStatus,
      paidCost,
      driverLocation,
      driverWallet,
      paidCostStatus,
    } = this.props;
    const average = 5;
    if (blockDriverStatus === "success" || deleteDriverStatus === "success")
      return <Navigate to="/mainpage/alldrivers" />;
    return (
      <React.Fragment>
        <Dialog
          open={this.state.openEditDriver}
          onClose={this.handleCloseEditDriver}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <EditDriver driver={driver} type="edit" />
        </Dialog>
        <Dialog
          open={this.state.openConfirmDialog}
          onClose={this.handleCloseConfirmDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth={true}
        >
          <ConfirmDialog
            handleCloseConfirmDialog={this.handleCloseConfirmDialog}
            handleSubmit={this.handleBlockDriver}
          />
        </Dialog>
        <BootstrapDialog
          onClose={this.handleCloseMapDialog}
          aria-labelledby="customized-dialog-title"
          maxWidth="md"
          fullWidth={true}
          open={this.state.openMapDialog}
        >
          <DriverMapComponent
            driverLocation={driverLocation}
            handleClose={this.handleCloseMapDialog}
          />
        </BootstrapDialog>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          style={{ marginTop: "5em" }}
        >
          <Grid item>
            <Avatar alt="Remy Sharp" src="" sx={{ width: 150, height: 150 }} />
          </Grid>
          <Grid item>
            <Typography variant="h2">
              {driver ? (driver.name ? driver.name : null) : null}
            </Typography>
          </Grid>
          {driverRate ? (
            <Grid item>
              {[...Array(driverRate)].map((star, i) => {
                const ratingValue = i + 1;
                return (
                  <label>
                    {" "}
                    <FaStar
                      className={classes.star}
                      color={ratingValue <= average ? "#2DBB54" : "#e4e5e9"}
                      size={25}
                    />
                  </label>
                );
              })}
            </Grid>
          ) : null}
          <Grid item>
            <Grid
              container
              direction="row"
              justifyContent="space-around"
              alignItems="center"
            >
              <Button
                color="primary"
                variant="contained"
                startIcon={<EditIcon />}
                onClick={this.handleOpenEditDriver}
              >
                Edit
              </Button>
              <Button
                color="primary"
                variant="contained"
                startIcon={<TimelineIcon />}
                onClick={this.handleOpenMapDialog}
                sx={{
                  marginInlineStart: "1.5em",
                  color: (theme) => theme.palette.primary.icons,
                  backgroundColor: (theme) => theme.palette.primary.primaryText,
                  "&:hover": {
                    color: (theme) => theme.palette.primary.icons,
                    backgroundColor: (theme) =>
                      theme.palette.primary.primaryText,
                  },
                }}
              >
                Tracking
              </Button>
              <Button
                variant="contained"
                startIcon={<BlockIcon />}
                sx={{
                  marginInlineStart: "1.5em",
                  backgroundColor: (theme) => theme.palette.primary.light,
                }}
                onClick={() => this.handleOpenConfirmDialog("block")}
              >
                Block
              </Button>
              <Button
                variant="contained"
                startIcon={<DeleteIcon />}
                sx={{
                  marginInlineStart: "1.5em",
                  color: (theme) => theme.palette.primary.icons,
                  backgroundColor: (theme) => theme.palette.primary.errColor,
                  "&:hover": {
                    color: (theme) => theme.palette.primary.icons,
                    backgroundColor: (theme) => theme.palette.primary.errColor,
                  },
                }}
                onClick={() => this.handleOpenConfirmDialog("delete")}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <TableHead style={{ width: "100%" }}>
          <TableRow className={classes.tableRow}>
            <Typography variant="h5">Personal Information</Typography>
          </TableRow>
        </TableHead>
        <Divider style={{ width: "100%" }} />
        <TableBody>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Name: {driver ? (driver.name ? driver.name : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                E-mail: {driver ? (driver.email ? driver.email : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Nationality Number:{" "}
                {driver
                  ? driver.nationality
                    ? driver.nationality
                    : null
                  : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Phone Number:{" "}
                {driver ? (driver.phone ? driver.phone : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Gender: {driver ? (driver.gender ? driver.gender : null) : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Car brand: {driverCar ? driverCar.brandName : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Car type: {driverCar ? driverCar.modelName : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Car number:{" "}
                {driverCar
                  ? driverCar.car
                    ? driverCar.car.carNumber
                    : null
                  : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Car color:{" "}
                {driverCar
                  ? driverCar.car
                    ? driverCar.car.carColor
                    : null
                  : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.tableCell}>
              <Typography variant="subtitle1">
                Certificate Number:{" "}
                {driver
                  ? driver.driverCertificateNumber
                    ? driver.driverCertificateNumber
                    : null
                  : null}
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableBody>
        <Typography variant="h5" style={{}}>
          Trips
        </Typography>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell align="center">Today</TableCell>
                <TableCell align="center">Last Week</TableCell>
                <TableCell align="center">This month</TableCell>
                <TableCell align="center">Last month</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Number of trips
                </TableCell>
                <TableCell align="center">{this.state.invoicesToday}</TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastWeek}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesThisMonth}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastMonth}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Revenue
                </TableCell>
                <TableCell align="center">{this.state.invoicesToday}</TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastWeek}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesThisMonth}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastMonth}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Company Net Revenue
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesTodayCompany}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastWeekCompany}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesThisMonthCompany}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastMonthCompany}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Driver Net Revenue
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesTodayDriver}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastWeekDriver}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesThisMonthDriver}
                </TableCell>
                <TableCell align="center">
                  {this.state.invoicesLastMonthDriver}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="flex-start"
          className={classes.containerGrid}
        >
          <Grid item>
            <Typography variant="subtitle2">
              Driver account balance:{" "}
              <Typography
                variant="h5"
                sx={{ color: (theme) => theme.palette.primary.main }}
              >
                {driverWallet ? driverWallet.paidCost : 0} SYP
              </Typography>
            </Typography>
          </Grid>
          <Grid item sx={{ marginTop: "1.5em" }}>
            <Typography variant="subtitle2">
              Enter how much you want to pay for the driver:{" "}
            </Typography>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              <Grid item>
                <TextField
                  size="small"
                  id="payAmount"
                  defaultValue={this.state.payAmount}
                  value={this.state.payAmount}
                  onChange={this.handleChange}
                  // className={classes.textField}
                />
              </Grid>
              <Grid item>
                <Typography variant="h6">SYP</Typography>
              </Grid>
              <Grid item>
                {paidCostStatus !== "loading" ? (
                  <Button
                    variant="contained"
                    onClick={() =>
                      this.handlePay(driverWallet ? driverWallet.id : null)
                    }
                  >
                    Pay
                  </Button>
                ) : (
                  <Button variant="contained">
                    <CircularProgress />
                  </Button>
                )}
              </Grid>
            </Grid>
          </Grid>

          <Grid item sx={{ marginTop: "1.5em" }}>
            <Typography variant="subtitle2">
              Add bonus for the driver:{" "}
            </Typography>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              <Grid item>
                <TextField
                  size="small"
                  // className={classes.textField}
                />
              </Grid>
              <Grid item>
                <Typography variant="h6">SYP</Typography>
              </Grid>
              <Grid item>
                <Button variant="contained">Pay</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    driver: state.drivers.oneDriver,
    driverRate: state.drivers.driverRate,
    driverCar: state.drivers.driverCar,
    blockDriverStatus: state.drivers.blockDriver,
    deleteDriverStatus: state.drivers.deleteDriver,
    invoicesToday: state.driverInvoices.invoicesToday,
    invoicesThisMonth: state.driverInvoices.invoicesThisMonth,
    invoicesLastWeek: state.driverInvoices.invoicesLastWeek,
    invoicesLastMonth: state.driverInvoices.invoicesLastMonth,
    paidCost: state.driverInvoices.paidCost,
    paidCostStatus: state.drivers.paidCostStatus,
    driverLocation: state.drivers.driverLocation,
    driverWallet: state.drivers.driverWallet
      ? state.drivers.driverWallet[0]
      : null,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getOneDriver: (driverId) => dispatch(getOneDriver(driverId)),
    getDriverCar: (driverId) => dispatch(getDriverCar(driverId)),
    blockOrUnBlockDriver: (driverId) =>
      dispatch(blockOrUnBlockDriver(driverId)),
    deleteDriver: (driverId) => dispatch(deleteDriver(driverId)),
    getRateForDriver: (driverId) => dispatch(getRateForDriver(driverId)),
    getTodayDriverInvoices: (driverId) =>
      dispatch(getTodayDriverInvoices(driverId)),
    getLastWeekDriverInvoices: (driverId) =>
      dispatch(getLastWeekDriverInvoices(driverId)),
    getDriverInvoicesThisMonth: (driverId) =>
      dispatch(getDriverInvoicesThisMonth(driverId)),
    getDriverInvoicesLastMonth: (driverId) =>
      dispatch(getDriverInvoicesLastMonth(driverId)),
    getPaidCost: (driverId) => dispatch(getPaidCost(driverId)),
    collectMoney: (paidCost, driverId) =>
      dispatch(collectMoney(paidCost, driverId)),
    getDriverWallet: (driverId) => dispatch(getDriverWallet(driverId)),
    getHistoryTracking: () => dispatch(getHistoryTracking()),
    clearPaidCostStatus: () => dispatch(clearPaidCostStatus()),
  };
};

export default withRouter(
  compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps)
  )(DriverDetails)
);
