import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import ImageUpload from "../images/ImageUpload";

const styles = (theme) => ({
  textField: {
    width: "100%",
  },
});

class EditDriver extends Component {
  constructor(props) {
    super(props);
    if (this.props.type === "add") {
      this.state = {
        name: "",
        email: "",
        gender: null,
        phone: null,
        nationality: null,
        driverCertificateNumber: null,
        imageProfile: null,
        id: props.driver ? props.driver.id : null,
      };
    } else {
      this.state = {
        name: props.driver ? props.driver.name : null,
        email: props.driver ? props.driver.email : null,
        gender: props.driver ? props.driver.gender : null,
        phone: props.driver ? props.driver.phone : null,
        nationality: props.driver ? props.driver.name : null,
        driverCertificateNumber: props.driver
          ? props.driver.driverCertificateNumber
          : null,
        imageProfile: props.driver ? props.driver.imageProfile : null,
        id: props.driver ? props.driver.id : null,
      };
    }
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeGender = (e) => {
    this.setState({
      gender: e.target.value,
    });
  };

  handleChangeImage = (imageProfile) => {
    this.setState({
      imageProfile: imageProfile,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{ color: (theme) => theme.palette.primary.main }}
        >
          {"Edit Drvier"}
        </DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <ImageUpload
              imageProfile={this.state.imageProfile}
              handleChangeImage={this.handleChangeImage}
            />
          </Grid>
          <TextField
            required
            id="name"
            label="Name"
            variant="standard"
            onChange={this.handleChange}
            value={this.state.name}
            className={classes.textField}
          />
          <TextField
            required
            id="email"
            label="E-mail"
            variant="standard"
            onChange={this.handleChange}
            value={this.state.email}
            className={classes.textField}
          />
          <TextField
            required
            id="nationality"
            label="Nationality Number"
            variant="standard"
            onChange={this.handleChange}
            value={this.state.nationality}
            className={classes.textField}
          />
          <TextField
            required
            id="driverCertificateNumber"
            label="Certificate Number"
            variant="standard"
            onChange={this.handleChange}
            value={this.state.driverCertificateNumber}
            className={classes.textField}
          />
          <TextField
            id="gender"
            select
            label="Gender"
            variant="standard"
            value={this.state.gender}
            onChange={(e) => this.handleChangeGender(e)}
            className={classes.textField}
          >
            <MenuItem value={"MALE"}>Male</MenuItem>
            <MenuItem value={"FEMALE"}>Female</MenuItem>
          </TextField>
        </DialogContent>
        <DialogActions>
          <Button autoFocus variant="contained" color="primary">
            {this.props.type === "add" ? "Add" : "Edit"}
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(EditDriver);
