import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
// import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import VisibilityIcon from "@mui/icons-material/Visibility";

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  visibilityIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
  fab: {
    margin: 0,
    top: "auto",
    right: 20,
    bottom: 20,
    left: "auto",
    position: "fixed",
    zIndex: 2,
  },
});

class AllDriversList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleOpenProfile = (driverId) => {
    window.open("/admindriverprofile/" + driverId);
  };
  render() {
    const { classes, driver } = this.props;
    return (
      <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
        <TableCell component="th" scope="row">
          {driver.name}
        </TableCell>
        <TableCell align="center">{driver.email}</TableCell>
        <TableCell align="center">{driver.nationality}</TableCell>
        <TableCell align="center">{driver.phone}</TableCell>
        <TableCell align="center">{driver.id}</TableCell>
        <TableCell align="center">
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={() => this.handleOpenProfile(driver.id)}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllDriversList);
