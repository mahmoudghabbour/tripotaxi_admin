import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import VisibilityIcon from "@mui/icons-material/Visibility";
import TripDetails from "../trips/TripDetails";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(0),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const styles = (theme) => ({
  visibilityIcon: {
    marginInlineStart: "0.5em",
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
});

class TripsByDriverList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDetailsDialog: false,
    };
  }

  handleOpenDetailsDialog = () => {
    this.setState({
      openDetailsDialog: true,
    });
  };

  handleCloseDetailsDialog = () => {
    this.setState({
      openDetailsDialog: false,
    });
  };

  render() {
    const { classes, trip } = this.props;
    return (
      <TableRow
        key={trip.id}
        sx={{
          "&:last-child td, &:last-child th": { border: 0 },
        }}
      >
        <BootstrapDialog
          onClose={this.handleCloseDetailsDialog}
          aria-labelledby="customized-dialog-title"
          maxWidth="md"
          fullWidth={true}
          open={this.state.openDetailsDialog}
        >
          <TripDetails
            trip={trip}
            handleClose={this.handleCloseDetailsDialog}
          />
        </BootstrapDialog>
        <TableCell component="th" scope="row">
          {trip.driver ? trip.driver.name : null}
        </TableCell>
        <TableCell align="center">
          {trip.tripRequest
            ? trip.tripRequest.passenger
              ? trip.tripRequest.passenger.name
              : null
            : null}
        </TableCell>
        <TableCell align="center">
          {trip.tripRequest
            ? trip.tripRequest.sourceLocation
              ? trip.tripRequest.sourceLocation.fullAddress
              : null
            : null}
        </TableCell>
        <TableCell align="center">
          {trip.tripRequest
            ? trip.tripRequest.destinationLocation
              ? trip.tripRequest.destinationLocation.fullAddress
              : null
            : null}
        </TableCell>
        <TableCell align="center">
          {trip.createdAt ? trip.createdAt : null}
        </TableCell>
        <TableCell align="center">
          {trip.tripRequest ? trip.tripRequest.type : null}
        </TableCell>
        <TableCell align="center">
          {trip.status ? trip.status : "Pending"}
        </TableCell>
        <TableCell>
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={this.handleOpenDetailsDialog}
          />
        </TableCell>
      </TableRow>
    );
  }
}

export default withStyles(styles)(TripsByDriverList);
