import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import TripDetails from "./TripDetails";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(0),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
});

class AllPassengerTrips extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDetailsDialog: false,
    };
  }

  handleOpenDetailsDialog = () => {
    this.setState({
      openDetailsDialog: true,
    });
  };

  handleCloseDetailsDialog = () => {
    this.setState({
      openDetailsDialog: false,
    });
  };

  render() {
    const { classes, trip } = this.props;
    return (
      <TableRow className={classes.tableRow}>
        <BootstrapDialog
          onClose={this.handleCloseDetailsDialog}
          aria-labelledby="customized-dialog-title"
          maxWidth="md"
          fullWidth={true}
          open={this.state.openDetailsDialog}
        >
          <TripDetails
            trip={trip}
            handleClose={this.handleCloseDetailsDialog}
          />
        </BootstrapDialog>
        <TableCell>
          {/* <div className={classes.flexSpaceBetween}>
            <div>
              <img
                className={classes.profileImage}
                src={""}
                align="left"
                alt={""}
              />
            </div>
            <div>
              <Typography
                variant="subtitle2"
                // style={{ margin: "25px 0px 0px 5px" }}
              >
                {trip.driver.name}
              </Typography>
            </div>
          </div> */}
        </TableCell>
        <TableCell>
          <div className={classes.flexSpaceBetween}>
            <div>
              <img
                className={classes.profileImage}
                src={""}
                align="left"
                alt={""}
              />
            </div>
            <div>
              <Typography
                variant="subtitle2"
                // style={{ margin: "25px 0px 0px 5px" }}
              >
                {trip.passenger.name}
              </Typography>
            </div>
          </div>
        </TableCell>
        <TableCell>
          <Typography variant="subtitle2">{trip.id}</Typography>
        </TableCell>
        <TableCell>{trip.createdAt}</TableCell>
        <TableCell>{trip.status}</TableCell>
        <TableCell>
          <Button
            variant="contained"
            sx={{
              color: (theme) => theme.palette.primary.icons,
            }}
            onClick={this.handleOpenDetailsDialog}
          >
            Details
          </Button>
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};
const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllPassengerTrips);
