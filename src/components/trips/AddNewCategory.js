import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import { withStyles } from "@mui/styles";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Typography from "@mui/material/Typography";
import {
  addCategory,
  editCategory,
} from "../../store/actions/CategoriesAction";

const styles = (theme) => ({
  gridItem: {
    width: "100%",
    marginTop: "1em",
  },
  textField: {
    width: "100%",
  },
  typography: {
    marginTop: "10em",
  },
});

class AddNewCategory extends Component {
  constructor(props) {
    super(props);
    if (props.type === "add")
      this.state = {
        categoryName: "",
        seatsNumber: "",
        minimumFareAtDay: "",
        minDay: "",
        kmDay: "",
        minimumFareAtNight: "",
        minNight: "",
        kmNight: "",
      };
    else
      this.state = {
        categoryName: props.category.categoryName
          ? props.category.categoryName
          : "",
        seatsNumber: props.category.seatsNumber
          ? props.category.seatsNumber
          : "",
        minimumFareAtDay: props.category.minimumFareAtDay
          ? props.category.minimumFareAtDay
          : 0,
        minDay: props.category.minDay ? props.category.minDay : 0,
        kmDay: props.category.kmDay ? props.category.kmDay : 0,
        minimumFareAtNight: props.category.minimumFareAtNight
          ? props.category.minimumFareAtNight
          : 0,
        minNight: props.category.minNight ? props.category.minNight : 0,
        kmNight: props.category.kmNight ? props.category.kmNight : 0,
      };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.addSuccessfully) this.props.handleClose();
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = () => {
    var updateCategory = {
      categoryName: this.state.categoryName ? this.state.categoryName : "",
      seatsNumber: parseInt(this.state.seatsNumber)
        ? parseInt(this.state.seatsNumber)
        : 0,
      minimumFareAtDay: this.state.minimumFareAtDay
        ? parseInt(this.state.minimumFareAtDay)
        : 0,
      minDay: this.state.minDay ? parseInt(this.state.minDay) : 0,
      kmDay: this.state.kmDay ? parseInt(this.state.kmDay) : 0,
      minimumFareAtNight: this.state.minimumFareAtNight
        ? parseInt(this.state.minimumFareAtNight)
        : 0,
      minNight: this.state.minNight ? parseInt(this.state.minNight) : 0,
      kmNight: this.state.kmNight ? parseInt(this.state.kmNight) : 0,
    };
    if (this.props.type === "add") this.props.addCategory(updateCategory);
    else {
      const categoryId = this.props.category.id;
      this.props.editCategory({ updateCategory, categoryId });
    }
  };

  render() {
    const { classes, addSuccessfully } = this.props;

    return (
      <React.Fragment>
        <DialogTitle
          id="responsive-dialog-title"
          sx={{ color: (theme) => theme.palette.primary.main }}
        >
          Add New Category
        </DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="categoryName"
                label="Category Name"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.categoryName}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="seatsNumber"
                label="Seats number"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.seatsNumber}
                className={classes.textField}
              />
            </Grid>
          </Grid>
          <Typography
            variant="h5"
            sx={{
              marginTop: "1em",
              color: (theme) => theme.palette.primary.main,
            }}
          >
            The fare during the day:
          </Typography>
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="minimumFareAtDay"
                label="Minimum fare"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.minimumFareAtDay}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="minDay"
                label="SYP/min"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.minDay}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="kmDay"
                label="SYP/km"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.kmDay}
                className={classes.textField}
              />
            </Grid>
          </Grid>
          <Typography
            variant="h5"
            sx={{
              marginTop: "1em",
              color: (theme) => theme.palette.primary.main,
            }}
          >
            The night fare:
          </Typography>
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="minimumFareAtNight"
                label="Minimum fare"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.minimumFareAtNight}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="minNight"
                label="SYP/min"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.minNight}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                required
                id="kmNight"
                label="SYP/km"
                variant="standard"
                onChange={this.handleChange}
                value={this.state.kmNight}
                className={classes.textField}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          {this.props.type === "add" ? (
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleSubmit}
            >
              Add
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleSubmit}
            >
              Edit
            </Button>
          )}
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    addSuccessfully: state.categories.addSuccessfully,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCategory: (category) => dispatch(addCategory(category)),
    editCategory: (category) => dispatch(editCategory(category)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AddNewCategory);
