import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import VisibilityIcon from "@mui/icons-material/Visibility";
import TripDetails from "./TripDetails";
import { getDriverCarLocation } from "../../store/actions/DriversAction";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(0),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const styles = (theme) => ({
  tableRow: {
    width: "100%",
  },
  tableCell: {
    backgroundColor: theme.palette.primary.background,
  },
  profileImage: {
    maxWidth: "5rem",
    maxHeight: "5rem",
  },
  button: {
    ...theme.typography.normalButton,
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  flexSpaceBetween: {
    display: "flex",
    justifySpace: "space-between",
  },
  visibilityIcon: {
    color: theme.palette.primary.main,
    "&:hover": { cursor: "pointer" },
  },
  usernameTableCell: {
    "&:hover": { cursor: "pointer" },
  },
});

class AllTripsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDetailsDialog: false,
    };
  }

  componentDidMount() {
    this.props.getDriverCarLocation();
  }

  handleOpenDetailsDialog = () => {
    this.setState({
      openDetailsDialog: true,
    });
  };

  handleCloseDetailsDialog = () => {
    this.setState({
      openDetailsDialog: false,
    });
  };

  render() {
    const { classes, trip, driverCarLocation } = this.props;
    return (
      <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
        <BootstrapDialog
          onClose={this.handleCloseDetailsDialog}
          aria-labelledby="customized-dialog-title"
          maxWidth="md"
          fullWidth={true}
          open={this.state.openDetailsDialog}
        >
          <TripDetails
            type="accepted"
            trip={trip}
            driverCarLocation={driverCarLocation}
            handleClose={this.handleCloseDetailsDialog}
          />
        </BootstrapDialog>
        <TableCell component="th" scope="row">
          {trip.driver.name}
        </TableCell>
        <TableCell align="center">{trip.tripRequest.passenger.name}</TableCell>
        <TableCell align="center">{trip.id}</TableCell>
        <TableCell align="center">{trip.createdAt}</TableCell>
        <TableCell align="center">{trip.status}</TableCell>
        <TableCell align="center">
          <VisibilityIcon
            className={classes.visibilityIcon}
            onClick={this.handleOpenDetailsDialog}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    driverCarLocation: state.drivers.driverCarLocation,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getDriverCarLocation: () => dispatch(getDriverCarLocation()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(AllTripsList);
