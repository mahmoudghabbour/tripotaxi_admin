import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@mui/styles";
import { makeStyles } from "@mui/styles";
import Grid from "@mui/material/Grid";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CircularProgress from "@mui/material/CircularProgress";
import CloseIcon from "@mui/icons-material/Close";
import { getAllEvents } from "../../store/actions/EventsAction";

const useStyles = makeStyles((theme) => ({
  dialogTitle: {
    color: theme.palette.primary.main,
  },
}));

const styles = (theme) => ({});

const BootstrapDialogTitle = (props) => {
  const classes = useStyles();
  const { children, onClose, ...other } = props;
  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} className={classes.dialogTitle}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

class TripEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstTime: false,
      allEvents: [],
    };
  }
  componentDidMount() {
    this.props.getAllEvents();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.allEvents && !this.state.firstTime) {
      var allEvents = [];
      for (let i = 0; i < nextProps.allEvents.length; i++) {
        if (nextProps.allEvents[i][2] === "354017118046289") {
          allEvents.push(nextProps.allEvents[i]);
        }
      }
      this.setState({
        firstTime: true,
        allEvents: allEvents,
      });
    }
  }
  render() {
    return (
      <React.Fragment>
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={this.props.handleClose}
        >
          Events
        </BootstrapDialogTitle>
        {this.state.allEvents.length !== 0 ? (
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            {this.state.allEvents &&
              this.state.allEvents.map((event) => {
                return (
                  <Grid item xs={6}>
                    <Card
                      sx={{
                        width: "100%",
                        minWidth: "100%",
                        marginTop: "0.5em",
                      }}
                    >
                      <CardContent>
                        <Typography
                          sx={{
                            fontSize: 14,
                            color: (theme) => theme.palette.primary.main,
                          }}
                          color="text.secondary"
                          gutterBottom
                          align="right"
                        >
                          {event[1]}
                        </Typography>
                        <Typography variant="h5" component="div" align="right">
                          {event[3]}
                        </Typography>
                        <Typography
                          variant="h5"
                          component="div"
                          align="right"
                          sx={{ color: (theme) => theme.palette.primary.main }}
                        >
                          {event[11]}
                        </Typography>
                        <Typography variant="body2" align="right">
                          {event[12]}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                );
              })}
          </Grid>
        ) : (
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <CircularProgress />
          </Grid>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    allEvents: state.events.allEvents,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllEvents: () => dispatch(getAllEvents()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(TripEvents);
