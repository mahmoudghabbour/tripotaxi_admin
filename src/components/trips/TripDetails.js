import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { makeStyles } from "@mui/styles";
// import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
// import DialogActions from "@mui/material/DialogActions";
import Grid from "@mui/material/Grid";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import MapComponent from "../map/MapComponent";
import TripMapComponent from "../map/TripMapComponent";
import TripEvents from "./TripEvents";
import { getHistoryTrip } from "../../store/actions/TripsActions";

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: 8,
  },
  dialogTitle: {
    color: theme.palette.primary.main,
  },
  gridItem: {
    width: "100%",
    marginTop: "0.5em",
  },
}));

const BootstrapDialogTitle = (props) => {
  const classes = useStyles();
  const { children, onClose, ...other } = props;
  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} className={classes.dialogTitle}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

function TripDetails(props) {
  const classes = useStyles();
  const { trip } = props;
  const [isOpen, setIsOpen] = React.useState(false);
  useEffect(() => {
    props.getHistoryTrip();
  }, []);

  const handleOpenTripEvents = () => {
    setIsOpen(true);
  };

  const handleCloseTripEvents = () => {
    setIsOpen(false);
  };
  return (
    <React.Fragment>
      <Dialog
        open={isOpen}
        onClose={handleCloseTripEvents}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
      >
        <TripEvents handleClose={handleCloseTripEvents} />
      </Dialog>
      <BootstrapDialogTitle
        id="customized-dialog-title"
        onClose={props.handleClose}
      >
        Trip Details
      </BootstrapDialogTitle>
      <DialogContent dividers sx={{ p: 0 }}>
        <Typography variant="subtitle2" className={classes.typography}>
          Trip ID: {trip.id}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Trip Status: {trip.status}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Trip Type: {trip.tripRequest.tripType}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Created At: {trip.createdAt}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Driver Name: {trip.driver.name}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Driver E-mail: {trip.driver.email ? trip.driver.email : null}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Driver Nationality Number: {trip.driver.nationality}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Driver Phone: {trip.driver.phone}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Driver Certificate Number: {trip.driver.driverCertificateNumber}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Passenger Name: {trip.tripRequest.passenger.name}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Passenger Phone: {trip.tripRequest.passenger.phone}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Passenger E-mail:{" "}
          {trip.tripRequest.passenger.email
            ? trip.tripRequest.passenger.email
            : null}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Passenger Phone: {trip.tripRequest.passenger.phone}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Passenger Gender: {trip.tripRequest.passenger.gender}
        </Typography>
        <Divider sx={{ width: "100%" }} />
        <Typography variant="subtitle2" className={classes.typography}>
          Location:
        </Typography>
        <Divider sx={{ width: "100%" }} />
        {props.type ? (
          props.type === "accepted" ? (
            <TripMapComponent
              isMarkerShown
              driverCarLocation={props.driverCarLocation}
            />
          ) : (
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item className={classes.gridItem}>
                <Grid
                  container
                  direction="column"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Button variant="contained" onClick={handleOpenTripEvents}>
                    Events
                  </Button>
                </Grid>{" "}
              </Grid>
              <Grid item className={classes.gridItem}>
                {props.tripHistory ? (
                  <MapComponent isMarkerShown tripHistory={props.tripHistory} />
                ) : (
                  <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ mt: "0.5em" }}
                  >
                    <CircularProgress />
                  </Grid>
                )}
              </Grid>
            </Grid>
          )
        ) : (
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem} sx={{ marginTop: "0.5em" }}>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Button variant="contained" onClick={handleOpenTripEvents}>
                  Events
                </Button>
              </Grid>
            </Grid>
            <Grid item className={classes.gridItem}>
              {props.tripHistory ? (
                <MapComponent isMarkerShown tripHistory={props.tripHistory} />
              ) : (
                <Grid
                  container
                  direction="row"
                  justifyContent="center"
                  alignItems="center"
                  sx={{ mt: "0.5em" }}
                >
                  <CircularProgress />
                </Grid>
              )}
            </Grid>
          </Grid>
        )}
      </DialogContent>
      {/* <DialogActions>
        <Button autoFocus onClick={handleClose}>
          Save changes
        </Button>
      </DialogActions> */}
    </React.Fragment>
  );
}

const mapStateToProps = (state, props) => {
  return {
    tripHistory: state.trips.tripHistory,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getHistoryTrip: () => dispatch(getHistoryTrip()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TripDetails);
