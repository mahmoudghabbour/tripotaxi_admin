const initState = {
  invoicesToday: null,
  invoicesLastWeek: null,
  invoicesThisMonth: null,
  invoicesLastMonth: null,
  paidCost: null,
};

const DriverInvoicesReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_DRIVER_INVOICES_TODAY_SUCCESS":
      return {
        ...state,
        // invoicesToday: action.res.data.data,
        invoicesToday: 100,
      };

    case "GET_DRIVER_INVOICES_TODAY_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_INVOICES_LAST_WEEK_SUCCESS":
      return {
        ...state,
        // invoicesLastWeek: action.res.data.data,
        invoicesLastWeek: 100,
      };

    case "GET_DRIVER_INVOICES_LAST_WEEK_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_INVOICES_THIS_MONTH_SUCCESS":
      return {
        ...state,
        // invoicesThisMonth: action.res.data.data,
        invoicesThisMonth: 100,
      };

    case "GET_DRIVER_INVOICES_THIS_MONTH_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_INVOICES_LAST_MONTH_SUCCESS":
      return {
        ...state,
        // invoicesLastMonth: action.res.data.data,
        invoicesLastMonth: 100,
      };

    case "GET_DRIVER_INVOICES_LAST_MONTH_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_PAID_COST_SUCCESS":
      return {
        ...state,
        paidCost: action.res.data.data,
      };

    case "GET_DRIVER_PAID_COST_ERROR":
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default DriverInvoicesReducer;
