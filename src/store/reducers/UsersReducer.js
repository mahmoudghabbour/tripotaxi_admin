const initState = {
  users: null,
  oneUser: null,
  blockUser: null,
  deleteUser: null,
};

const UsersReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_USERS_SUCCESS":
      return {
        ...state,
        blockUser: null,
        users: action.res.data.data,
      };
    case "GET_ALL_USERS_ERROR":
      return {
        ...state,
      };

    case "GET_ONE_USER_SUCCESS":
      return {
        ...state,
        oneUser: action.res.data.data,
      };
    case "GET_ONE_USER_ERROR":
      return {
        ...state,
      };

    case "BLOCK_USER_SUCCESS":
      return {
        ...state,
        blockUser: "success",
      };
    case "BLOCK_USER_ERROR":
      return {
        ...state,
      };

    case "DELETE_USER_SUCCESS":
      return {
        ...state,
        deleteUser: "success",
      };
    case "DELETE_USER_ERROR":
      return {
        ...state,
      };

    default:
      return state;
  }
};

export default UsersReducer;
