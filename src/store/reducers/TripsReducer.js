const initState = {
  typesTrips: null,
  acceptedTrips: null,
  driverTrips: null,
  passengerTrips: null,
  tripHistory: null,
};

const TripsReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_DRIVER_TRIPS_SUCCESS":
      return {
        ...state,
        driverTrips: action.res.data.data,
      };
    case "GET_ALL_DRIVER_TRIPS_ERROR":
      return {
        ...state,
      };
    case "GET_ALL_PASSENGERS_TRIPS_SUCCESS":
      return {
        ...state,
        passengerTrips: action.res.data.data,
      };
    case "GET_ALL_PASSENGERS_TRIPS_ERROR":
      return {
        ...state,
      };

    case "GET_ALL_FILTERED_COMPLETED_TRIPS_SUCCESS":
      return {
        ...state,
        driverTrips: action.res.data.data,
      };
    case "GET_ALL_FILTERED_COMPLETED_TRIPS_ERROR":
      return {
        ...state,
      };

    case "GET_ALL_TYPES_TRIPS_SUCCESS":
      return {
        ...state,
        typesTrips: action.res.data.data,
      };
    case "GET_ALL_TYPES_TRIPS_ERROR":
      return {
        ...state,
      };

    case "GET_ALL_ACCEPTED_TRIPS_SUCCESS":
      return {
        ...state,
        acceptedTrips: action.res.data.data,
      };
    case "GET_ALL_ACCEPTED_TRIPS_ERROR":
      return {
        ...state,
      };

    case "GET_HISTORY_TRIP_SUCCESS":
      return {
        ...state,
        tripHistory: action.res.data.route,
      };
    case "GET_HISTORY_TRIP_ERROR":
      return {
        ...state,
      };

    default:
      return state;
  }
};

export default TripsReducer;
