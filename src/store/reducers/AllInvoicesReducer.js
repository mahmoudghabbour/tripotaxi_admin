const initState = {
  invoicesToday: null,
  invoicesLastWeek: null,
  invoicesThisMonth: null,
  invoicesLastMonth: null,
};

const InvoicesReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_INVOICES_TODAY_SUCCESS":
      return {
        ...state,
        // invoicesToday: action.res.data.data,
        invoicesToday: 100,
      };

    case "GET_INVOICES_TODAY_ERROR":
      return {
        ...state,
      };

    case "GET_INVOICES_LAST_WEEK_SUCCESS":
      return {
        ...state,
        // invoicesLastWeek: action.res.data.data,
        invoicesLastWeek: 100,
      };

    case "GET_INVOICES_LAST_WEEK_ERROR":
      return {
        ...state,
      };

    case "GET_INVOICES_THIS_MONTH_SUCCESS":
      return {
        ...state,
        // invoicesThisMonth: action.res.data.data,
        invoicesThisMonth: 100,
      };

    case "GET_INVOICES_THIS_MONTH_ERROR":
      return {
        ...state,
      };

    case "GET_INVOICES_LAST_MONTH_SUCCESS":
      return {
        ...state,
        // invoicesLastMonth: action.res.data.data,
        invoicesLastMonth: 100,
      };

    case "GET_INVOICES_LAST_MONTH_ERROR":
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default InvoicesReducer;
