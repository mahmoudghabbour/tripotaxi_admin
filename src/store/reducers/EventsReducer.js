const initState = {
  allEvents: null,
};

const EventsReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_EVENTS_SUCCESS":
      return {
        ...state,
        allEvents: action.res.data,
      };
    case "GET_ALL_EVENTS_ERROR":
      return {
        ...state,
      };

    default:
      return state;
  }
};

export default EventsReducer;
