import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import DriversReducer from "./DriversReducer";
import UsersReducer from "./UsersReducer";
import TripsReducer from "./TripsReducer";
import DriverInvoicesReducer from "./DriverInvoicesReducer";
import CategoriesReducer from "./CategoriesReducer";
import InvoicesReducer from "./AllInvoicesReducer";
import EventsReducer from "./EventsReducer";
const rootReducer = combineReducers({
  auth: AuthReducer,
  events: EventsReducer,
  drivers: DriversReducer,
  users: UsersReducer,
  trips: TripsReducer,
  allInvoices: InvoicesReducer,
  driverInvoices: DriverInvoicesReducer,
  categories: CategoriesReducer,
});

export default rootReducer;
