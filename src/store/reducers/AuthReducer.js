const initState = {
  auth: null,
  authErrorSignIn: null,
  signInSpinner: null,
  signOut: null,
};

const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case "LOGIN_ERROR":
      return {
        ...state,
        signInSpinner: null,
        authErrorSignIn: "Your E-mail or password is incorrect",
      };
    case "LOGIN_SUCCESS":
      var userDetails = {
        admin: action.res.data.data.admin,
        token: action.res.data.data.token,
      };
      var userToken = action.res.data.data.token;
      localStorage.setItem(
        "userDetails",
        JSON.stringify(action.res.data.data.admin)
      );
      localStorage.setItem("userToken", userToken);
      return {
        ...state,
        auth: userDetails,
        authErrorSignIn: null,
        signInSpinner: "success",
      };
    case "SIGN_OUT_SUCCESS":
      return {
        ...state,
        auth: null,
        signOut: "success",
      };
    case "SIGN_OUT_ERROR":
      return {
        ...state,
        authErrorSignIn: null,
      };
    case "SIGN_IN_SPINNER":
      return {
        ...state,
        signInSpinner: "loading",
      };
    default:
      return state;
  }
};

export default AuthReducer;
