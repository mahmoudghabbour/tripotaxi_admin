const initState = {
  drivers: null,
  driverCar: null,
  driverCarLocation: null,
  draiverRate: null,
  driverLocation: null,
  oneDriver: null,
  blockDriver: null,
  deleteDriver: null,
  driverWallet: null,
  paidCostStatus: null,
};

const DriversReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_DRIVERS_SUCCESS":
      return {
        ...state,
        blockDriver: null,
        drivers: action.res.data.data,
      };
    case "GET_ALL_DRIVERS_ERROR":
      return {
        ...state,
      };

    case "GET_CAR_DRIVER_SUCCESS":
      return {
        ...state,
        driverCar: action.res.data.data,
      };
    case "GET_CAR_DRIVER_ERROR":
      return {
        ...state,
      };

    case "GET_ONE_DRIVER_SUCCESS":
      return {
        ...state,
        oneDriver: action.res.data.data,
      };
    case "GET_ONE_DRIVER_ERROR":
      return {
        ...state,
      };

    case "BLOCK_DRIVER_SUCCESS":
      return {
        ...state,
        blockDriver: "success",
      };
    case "BLOCK_DRIVER_ERROR":
      return {
        ...state,
      };
    case "DELETE_DRIVER_SUCCESS":
      return {
        ...state,
        deleteDriver: "success",
      };
    case "DELETE_DRIVER_ERROR":
      return {
        ...state,
      };
    case "GET_RATE_SUCCESS":
      return {
        ...state,
        driverRate: action.res.data.data,
      };
    case "GET_RATE_ERROR":
      return {
        ...state,
      };

    case "GET_HISTORY_TRACKING_SUCCESS":
      return {
        ...state,
        driverLocation: action.res.data.route,
      };
    case "GET_HISTORY_TRACKING_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_CAR_LOCATION_SUCCESS":
      return {
        ...state,
        driverCarLocation: action.res.data[354017113745240],
      };
    case "GET_DRIVER_CAR_LOCATION_ERROR":
      return {
        ...state,
      };

    case "GET_DRIVER_WALLET_SUCCESS":
      return {
        ...state,
        driverWallet: action.res.data.data,
      };
    case "GET_DRIVER_WALLET_ERROR":
      return {
        ...state,
      };

    case "COLLECT_MONEY_SUCCESS":
      return {
        ...state,
        paidCostStatus: "success",
      };
    case "COLLECT_MONEY_ERROR":
      return {
        ...state,
        paidCostStatus: "error",
      };
    case "COLLECT_MONEY_LOADING":
      return {
        ...state,
        paidCostStatus: "loading",
      };
    case "CLEAR_PAID_COST_STATUS":
      return {
        ...state,
        paidCostStatus: null,
      };

    default:
      return state;
  }
};

export default DriversReducer;
