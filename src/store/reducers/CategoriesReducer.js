const initState = {
  categories: null,
  addSuccessfully: false,
  deleteCategory: null,
};

const CategoriesReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_CATEGORIES_SUCCESS":
      return {
        ...state,
        addSuccessfully: false,
        categories: action.res.data.data,
      };
    case "GET_ALL_CATEGORIES_ERROR":
      return {
        ...state,
      };

    case "ADD_NEW_CATEGORY_SUCCESS":
      return {
        ...state,
        addSuccessfully: true,
      };
    case "ADD_NEW_CATEGORY_ERROR":
      return {
        ...state,
      };

    case "EDIT_CATEGORY_SUCCESS":
      return {
        ...state,
        addSuccessfully: true,
      };
    case "EDIT_CATEGORY_ERROR":
      return {
        ...state,
      };

    case "DELETE_CATEGORY_SUCCESS":
      return {
        ...state,
        deleteCategory: "success",
      };
    case "DELETE_CATEGORY_ERROR":
      return {
        ...state,
        deleteCategory: "error",
      };
    case "DELETE_CATEGORY_LOADING":
      return {
        ...state,
        deleteCategory: "loading",
      };

    default:
      return state;
  }
};

export default CategoriesReducer;
