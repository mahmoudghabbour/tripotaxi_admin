import axios from "axios";

export const getAllUsers = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-all-Un-BlockPassenger", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_USERS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_USERS_ERROR", err });
      });
  };
};

export const getAllBlockedUsers = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/getAllBlockAccountPassenger", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_USERS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_USERS_ERROR", err });
      });
  };
};

export const getOneUser = (userId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(`http://localhost:3000/admin/get-passenger?passengerId=${userId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ONE_USER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ONE_USER_ERROR", err });
      });
  };
};

export const blockOrUnBlockUser = (user) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .put(
        `http://localhost:3000/admin/updateStatusAccountPassenger?passengerId=${user.id}`,
        user,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "My-Custom-Header": "foobar",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "BLOCK_USER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "BLOCK_USER_ERROR", err });
      });
  };
};

export const deleteUser = (userId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .delete(
        `http://localhost:3000/admin/delete-passenger?passengerId=${userId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "DELETE_USER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "DELETE_USER_ERROR", err });
      });
  };
};
