import axios from "axios";

export const getTodayInvoices = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-all-invoices-by-all-drivers?today=true`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_INVOICES_TODAY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_INVOICES_TODAY_ERROR", err });
      });
  };
};

export const getLastWeekInvoices = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-all-invoices-by-all-drivers?lastWeek=true`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_INVOICES_LAST_WEEK_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_INVOICES_LAST_WEEK_ERROR", err });
      });
  };
};

export const getInvoicesThisMonth = () => {
  return (dispatch) => {
    var startDate =
      new Date().getMonth() + 1 + "-" + 1 + "-" + new Date().getFullYear();
    var endDate =
      new Date().getMonth() +
      1 +
      "-" +
      new Date().getDate() +
      "-" +
      new Date().getFullYear();
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-all-invoices-by-all-drivers?startDate=${startDate}&endDate=${endDate}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_INVOICES_THIS_MONTH_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_INVOICES_THIS_MONTH_ERROR", err });
      });
  };
};

export const getInvoicesLastMonth = () => {
  return (dispatch) => {
    var startDate =
      new Date().getMonth() + "-" + 1 + "-" + new Date().getFullYear();
    var endDate =
      new Date().getMonth() +
      "-" +
      new Date(new Date().getFullYear(), new Date().getMonth(), 0).getDate() +
      "-" +
      new Date().getFullYear();
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-all-invoices-by-all-drivers?startDate=${startDate}&endDate=${endDate}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_INVOICES_LAST_MONTH_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_INVOICES_LAST_MONTH_ERROR", err });
      });
  };
};
