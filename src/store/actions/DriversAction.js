import axios from "axios";
import { constantsConfig } from "../../config/ConstantsConfig";

export const getAllDrivers = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-all-Un-BlockDriver", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_DRIVERS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_DRIVERS_ERROR", err });
      });
  };
};

export const getDriverCar = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/admin/get-car-by-driver?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_CAR_DRIVER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_CAR_DRIVER_ERROR", err });
      });
  };
};

export const getAllBlockedDrivers = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/getAllBlockAccountDriver", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_DRIVERS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_DRIVERS_SUCCESS", err });
      });
  };
};

export const getOneDriver = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(`http://localhost:3000/admin/get-driver?driverId=${driverId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ONE_DRIVER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ONE_DRIVER_ERROR", err });
      });
  };
};

export const blockOrUnBlockDriver = (driver) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .put(
        `http://localhost:3000/admin/updateStatusAccountDriver?driverId=${driver.id}`,
        driver,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "My-Custom-Header": "foobar",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "BLOCK_DRIVER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "BLOCK_DRIVER_ERROR", err });
      });
  };
};

export const deleteDriver = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .delete(
        `http://localhost:3000/admin/delete-driver?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "DELETE_DRIVER_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "DELETE_DRIVER_ERROR", err });
      });
  };
};

export const getRateForDriver = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/admin/get-all-rateDriver?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_RATE_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_RATE_ERROR", err });
      });
  };
};

export const getDriverWallet = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-driver-all-invoice?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_WALLET_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_WALLET_ERROR", err });
      });
  };
};

export const collectMoney = (paidCost, invoiceId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    dispatch({ type: "COLLECT_MONEY_LOADING" });
    axios
      .put(
        `http://localhost:3000/invoice/pay-admin?invoiceId=${invoiceId}`,
        { payAdmin: paidCost },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "My-Custom-Header": "foobar",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "COLLECT_MONEY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "COLLECT_MONEY_ERROR", err });
      });
  };
};

export const clearPaidCostStatus = () => {
  return (dispatch) => {
    dispatch({ type: "CLEAR_PAID_COST_STATUS" });
  };
};

export const getHistoryTracking = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        "http://admin.alather.net/api/api.php?api=user&ver=1.0&key=6EE81E432ADEF72D8812C7927B8F40FA&cmd=OBJECT_GET_ROUTE,352625696248036,2022-06-07%2010:00:00,2022-06-07%2020:30:59,20.0",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_HISTORY_TRACKING_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_HISTORY_TRACKING_ERROR", err });
      });
  };
};

export const getDriverCarLocation = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        "https://admin.alather.net/api/api.php?api=user&ver=1.0&key=6EE81E432ADEF72D8812C7927B8F40FA&cmd=OBJECT_GET_LOCATIONS,354017113745240",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_CAR_LOCATION_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_CAR_LOCATION_ERROR", err });
      });
  };
};
