import axios from "axios";

export const getAllCategories = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-all-categories", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_CATEGORIES_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_CATEGORIES_ERROR", err });
      });
  };
};

export const addCategory = (category) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .post("http://localhost:3000/category", category, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "ADD_NEW_CATEGORY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "ADD_NEW_CATEGORY_ERROR", err });
      });
  };
};

export const editCategory = (category) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .put(
        `http://localhost:3000/category?categoryId=${category.categoryId}`,
        category.updateCategory,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "My-Custom-Header": "foobar",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "EDIT_CATEGORY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "EDIT_CATEGORY_ERROR", err });
      });
  };
};

export const deleteCategory = (categoryId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    dispatch({ type: "DELETE_CATEGORY_LOADING" });
    axios
      .delete(`http://localhost:3000/category?categoryId=${categoryId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "DELETE_CATEGORY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "DELETE_CATEGORY_ERROR", err });
      });
  };
};
