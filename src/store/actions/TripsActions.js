import axios from "axios";

export const getAllDriverTrips = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-all-trips-driver", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_DRIVER_TRIPS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_DRIVER_TRIPS_ERROR", err });
      });
  };
};

export const getTripsByDriver = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/admin/get-all-trips-oneDriver?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_ALL_DRIVER_TRIPS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_DRIVER_TRIPS_ERROR", err });
      });
  };
};

export const getTripsByPassenger = (passengerId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/admin/get-all-tripRequest-onePassenger?passengerId=${passengerId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_ALL_PASSENGERS_TRIPS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_PASSENGERS_TRIPS_ERROR", err });
      });
  };
};

export const getFilteredCompletedTrips = (info) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    if (info.driverId && !info.categoryName) {
      axios
        .post(
          `http://localhost:3000/admin/get-filter-trip?driverId=${info.driverId}`,
          { name: info.driverName },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_SUCCESS", res });
        })
        .catch((err) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_ERROR", err });
        });
    } else if (!info.driverId && info.categoryName) {
      axios
        .post(
          `http://localhost:3000/admin/get-filter-trip?type=${info.categoryName}`,
          {},
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_SUCCESS", res });
        })
        .catch((err) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_ERROR", err });
        });
    } else {
      axios
        .post(
          `http://localhost:3000/admin/get-filter-trip?type=${info.categoryName}&driverId=${info.driverId}`,
          { name: info.driverName },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_SUCCESS", res });
        })
        .catch((err) => {
          dispatch({ type: "GET_ALL_FILTERED_COMPLETED_TRIPS_ERROR", err });
        });
    }
  };
};

export const getAllTypesTrips = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-type-trip", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_TYPES_TRIPS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_TYPES_TRIPS_ERROR", err });
      });
  };
};

export const getAllAcceptedTrips = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get("http://localhost:3000/admin/get-all-accepted-trips", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        dispatch({ type: "GET_ALL_ACCEPTED_TRIPS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_ACCEPTED_TRIPS_ERROR", err });
      });
  };
};

export const getHistoryTrip = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        "http://admin.alather.net/api/api.php?api=user&ver=1.0&key=6EE81E432ADEF72D8812C7927B8F40FA&cmd=OBJECT_GET_ROUTE,352625696248036,2022-08-01%2010:00:00,2022-08-01%2020:30:59,20.0",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_HISTORY_TRIP_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_HISTORY_TRIP_ERROR", err });
      });
  };
};
