import axios from "axios";

export const getTodayDriverInvoices = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-driver-all-invoice?driverId=${driverId}&today=true`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_INVOICES_TODAY_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_INVOICES_TODAY_ERROR", err });
      });
  };
};

export const getLastWeekDriverInvoices = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-driver-all-invoice?driverId=${driverId}&lastWeek=true`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_INVOICES_LAST_WEEK_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_INVOICES_LAST_WEEK_ERROR", err });
      });
  };
};

export const getDriverInvoicesThisMonth = (driverId) => {
  return (dispatch) => {
    var startDate =
      new Date().getMonth() + 1 + "-" + 1 + "-" + new Date().getFullYear();
    var endDate =
      new Date().getMonth() +
      1 +
      "-" +
      new Date().getDate() +
      "-" +
      new Date().getFullYear();
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-driver-all-invoice?driverId=${driverId}&startDate=${startDate}&endDate=${endDate}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_INVOICES_THIS_MONTH_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_INVOICES_THIS_MONTH_ERROR", err });
      });
  };
};

export const getDriverInvoicesLastMonth = (driverId) => {
  return (dispatch) => {
    var startDate =
      new Date().getMonth() + "-" + 1 + "-" + new Date().getFullYear();
    var endDate =
      new Date().getMonth() +
      "-" +
      new Date(new Date().getFullYear(), new Date().getMonth(), 0).getDate() +
      "-" +
      new Date().getFullYear();
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-driver-all-invoice?driverId=${driverId}&startDate=${startDate}&endDate=${endDate}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_INVOICES_LAST_MONTH_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_INVOICES_LAST_MONTH_ERROR", err });
      });
  };
};

export const getPaidCost = (driverId) => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        `http://localhost:3000/invoice/get-all-paid-cost?driverId=${driverId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_DRIVER_PAID_COST_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_DRIVER_PAID_COST_ERROR", err });
      });
  };
};
