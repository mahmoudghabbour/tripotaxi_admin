import axios from "axios";

export const getAllEvents = () => {
  return (dispatch) => {
    const token = localStorage.getItem("userToken");
    axios
      .get(
        "http://admin.alather.net/api/api.php?api=user&ver=1.0&key=A7EE9AB5CF5EE82C6EC4C86E6FEA0185&cmd=OBJECT_GET_LAST_EVENTS",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        dispatch({ type: "GET_ALL_EVENTS_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "GET_ALL_EVENTS_ERROR", err });
      });
  };
};
