import axios from "axios";

export const signIn = (cred) => {
  return (dispatch) => {
    dispatch({ type: "SIGN_IN_SPINNER"});
    axios
      .post("http://localhost:3000/auth-admin/login", cred)
      .then((res) => {
        dispatch({ type: "LOGIN_SUCCESS", res });
      })
      .catch((err) => {
        dispatch({ type: "LOGIN_ERROR", err });
      });
  };
};

export const signOut = () => {
  return (dispatch) => {
    localStorage.removeItem("userDetails");
    localStorage.removeItem("userToken");
    dispatch({ type: "SIGN_OUT_SUCCESS" });
  };
};
